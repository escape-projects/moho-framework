import sys

import os.path
from distutils.version import LooseVersion
from setuptools import setup, Extension, find_packages

import numpy as np
import moho


DESCRIPTION = "Moho framework"

PACKAGE_NAME = "Moho"

VERSION = moho.__version__

URL = ""

LICENSE = "GPLv3"

AUTHOR = 'Denis Korolkov, Stepan Rakhimov'

AUTHOR_EMAIL = 'escape.app.net@gmail.com'

INSTALL_REQUIREMENTS = ["Cython>=x0.27", "tornado>x=6.0", "matplotlib>=3.3.1"]

if sys.version_info < (3, 5):
    print('ERROR: Moho requires Python >= 3.5 to run.')
    sys.exit(1)


try:
    # check for cython
    from Cython.Distutils import build_ext
    import Cython
except ImportError:
    print('\nCython is missing, its required for compiling Moho !\n\n')
    raise

include_dirs = []
library_dirs = []
extra_compile_args = []
#
if sys.platform.startswith('linux'):
    include_dirs.append(np.get_include())
elif sys.platform.startswith('darwin'):
    include_dirs.append(np.get_include())
    include_dirs.append(os.path.join("/opt", "local", "include"))
elif sys.platform.startswith('win32'):
    include_dirs.append(np.get_include())


ext_modules = [Extension("moho.core._event",
                         sources=[
                             os.path.join(".", "moho", "core", "_event.pyx")],
                         depends=['properties.pxd'],
                         language="c",
                         include_dirs=include_dirs,
                         library_dirs=library_dirs,
                         extra_compile_args=extra_compile_args,
                         ),
               Extension("moho.core._clock",
                         sources=[
                             os.path.join(".", "moho", "core", "_clock.pyx")],
                         language="c",
                         include_dirs=include_dirs,
                         library_dirs=library_dirs,
                         extra_compile_args=extra_compile_args,
                         ),
               Extension("moho.core.weakproxy",
                         sources=[
                             os.path.join(".", "moho", "core", "weakproxy.pyx")],
                         language="c",
                         include_dirs=include_dirs,
                         library_dirs=library_dirs,
                         extra_compile_args=extra_compile_args,
                         ),
               Extension("moho.core._attributes",
                         sources=[
                             os.path.join(".", "moho", "core", "_attributes.pyx")],
                         depends=['properties.pxd'],
                         language="c",
                         include_dirs=include_dirs,
                         library_dirs=library_dirs,
                         extra_compile_args=extra_compile_args,
                         ),
               Extension("moho.core.properties",
                         sources=[
                             os.path.join(".", "moho", "core", "properties.pyx")],
                         depends=['_event.pxd'],
                         language="c",
                         include_dirs=include_dirs,
                         library_dirs=library_dirs,
                         extra_compile_args=extra_compile_args,
                         ),
               ]


NAMESPACE_PACKAGES = ['moho']

setup(
    name=PACKAGE_NAME,
    version=VERSION,
    url=URL,
    license=LICENSE,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=DESCRIPTION,
    zip_safe=False,
    ext_modules=ext_modules,
    cmdclass={"build_ext": build_ext},
    package_dir={'moho': 'moho'},
    package_data={'moho': [
        '*.pxd',
        '*.pxi',
        'data/*.mh',
        'data/*.json',
        'data/fonts/*.ttf',
        'data/images/*.png',
        'data/images/*.jpg',
        'data/images/*.gif',
        'data/images/*.atlas',
        'data/keyboards/*.json',
        'data/logo/*.png',
        'data/glsl/*.png',
        'data/glsl/*.vs',
        'data/glsl/*.fs',
    ]},
    data_files=[],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    install_requires=INSTALL_REQUIREMENTS,
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "moho-admin = moho.admin",
        ],
    },
)


# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
