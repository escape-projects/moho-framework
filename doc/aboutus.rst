
About Us
-----------

Moho is an open-source framework for creating of web-applications and is distributed under GPLv3 license.
If you need another license, please write us a short email to 
escape.app.net@gmail.com, telling us few words about you and your project. 

Core developers:

1. ``Denis Korolkov``

   * After finishing his PhD in physics and postdoc contract in Jülich Centre for Neutron Science in Germany,
     he works in industry as a software developer of analytical software.
     His main interests are non-destructive testing methods and metrology. Denis lives in Germany.  


2. ``Stepan Rakhimov``

   * By education Stepan is a PhD in theoretical physics with professional knowledge in software development and DevOps.
     Stepan lives in the Netherlands.

