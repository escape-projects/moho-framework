'''
Extension to add lexer support for mh files
'''


def setup(app):
    from highlight import MohoLexer

    app.add_lexer('mh', MohoLexer)
    
