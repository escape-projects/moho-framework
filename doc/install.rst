
Installation
----------------------------

ESCAPE Package: 

Choose and download `here <_static/download>`_ the latest version of ESCAPE package. Choose an appropriate wheel file
according to your operating system and installed Python version you have.
For example, wheel *ESCAPE-0.8.0.dev0-7598-cp35-cp35m-linux_x86_64.whl* is for 
64 bit linux and Python version 3.5.

After downloading wheel run the following command in the shell, (use *sudo* under linux if necessary)::

    pip3 install -U /path/to/your/wheel_file.whl


This command will install a version with minimal dependencies. It is useful if you are going to use
Python only. If you plan to use Jupyter notebooks, please run the following command after installation::

    pip3 install -U /path/to/your/wheel_file.whl[jupyter]

This command will install extra dependencies.


.. note::

    In the case if you have error messages like "DLL load failed" when importing ESCAPE package in your python code under Windows, 
    please install `Visual Studio 2017 redistributable <https://aka.ms/vs/16/release/vc_redist.x64.exe>`_.


