'''
Script to generate Moho API from source code.

'''

ignore_list = (
    'moho.core._clock',
    'moho.core._event',
    'moho.core.compat',
    'moho.core.context',
    'moho.core._event',
    'moho.core._attributes',
    
)

import os
import sys
from glob import glob

import moho

# force loading of kivy modules
import moho.core.app
import moho.uix

#import moho.uix.html5
import moho.uix.html5.button
import moho.uix.html5.div
import moho.uix.html5.heading
import moho.uix.html5.hyperlink
import moho.uix.html5.image
import moho.uix.html5.input
import moho.uix.html5.linebreak
import moho.uix.html5.paragraph
import moho.uix.html5.script
import moho.uix.html5.select
import moho.uix.html5.table
import moho.uix.html5.textarea

#import moho.uix.bootstrap
import moho.uix.bootstrap.app
import moho.uix.bootstrap.accordion
import moho.uix.bootstrap.alert
import moho.uix.bootstrap.boxlayout
import moho.uix.bootstrap.button
import moho.uix.bootstrap.card
import moho.uix.bootstrap.checkbox
import moho.uix.bootstrap.combobox
import moho.uix.bootstrap.dropdown
import moho.uix.bootstrap.input
import moho.uix.bootstrap.lineedit
import moho.uix.bootstrap.modal
import moho.uix.bootstrap.progress
import moho.uix.bootstrap.radio
import moho.uix.bootstrap.spinner
import moho.uix.bootstrap.tabs
import moho.uix.bootstrap.textedit
import moho.uix.bootstrap.toolbar

import moho.uix.bootstrap.mpl
import moho.uix.bootstrap.mpl.figure
import moho.uix.bootstrap.mpl.bar
import moho.uix.bootstrap.mpl.errorbar
import moho.uix.bootstrap.mpl.map3d
import moho.uix.bootstrap.mpl.plot2d
import moho.uix.bootstrap.mpl.scatter

from moho.core.factory import Factory

# force loading of all classes from factory
for x in list(Factory.classes.keys())[:]:
    getattr(Factory, x)

# Directory of doc
base_dir = os.path.dirname(__file__)
dest_dir = os.path.join(base_dir, 'rst', 'api')
examples_framework_dir = os.path.join(base_dir, '..', 'examples', 'framework')

# Check touch file
base = 'autobuild.py-done'
with open(os.path.join(base_dir, base), 'w') as f:
    f.write('')


def writefile(filename, data, dest_dir="."):
    # avoid to rewrite the file if the content didn't change
    f = os.path.join(dest_dir, filename)
    print('write', filename)
    if os.path.exists(f):
        with open(f) as fd:
            if fd.read() == data:
                return
    h = open(f, 'w')
    h.write(data)
    h.close()


# Search all moho module
l = [(x, sys.modules[x],
      os.path.basename(sys.modules[x].__file__).rsplit('.', 1)[0])
     for x in sys.modules if x.startswith('moho') and sys.modules[x]]


# Extract packages from modules
packages = []
modules = {}
api_modules = []
for name, module, filename in l:
    if name in ignore_list:
        continue
    if not any([name.startswith(x) for x in ignore_list]):
        api_modules.append(name)
    if filename == '__init__':
        packages.append(name)
    else:
        if hasattr(module, '__all__'):
            modules[name] = module.__all__
        else:
            modules[name] = [x for x in dir(module) if not x.startswith('__')]

packages.sort()


# Create index
api_index = '''API Reference
-------------

The API reference is a lexicographic list of all the different classes,
methods and features that Moho offers.

.. toctree::
    :maxdepth: 1

'''
api_modules.sort()
for package in packages:
    if len(package.rsplit("."))<=2:
        api_index += "    ./rst/api/api-%s.rst\n" % package

writefile('contents.rst', api_index)


# Create index for all packages
# Note on displaying inherited members;
#     Adding the directive ':inherited-members:' to automodule achieves this
#     but is not always desired. 

template = '\n'.join((
    '=' * 100,
    '$SUMMARY',
    '=' * 100,
    '''
$EXAMPLES_REF

.. automodule:: $PACKAGE
    :members:
    :show-inheritance:

.. toctree::

$EXAMPLES
'''))

template_module = '''
$EXAMPLES_REF

.. automodule:: $PACKAGE
    :members:
    :show-inheritance:

.. toctree::

$EXAMPLES
'''


template_examples = '''.. _example-reference%d:

Examples
--------

%s
'''

template_examples_ref = ('# :ref:`Jump directly to Examples'
                         ' <example-reference%d>`')


def extract_summary_line(doc):
    """
    :param doc: the __doc__ field of a module
    :return: a doc string suitable for a header or empty string
    """
    if doc is None:
        return ''
    for line in doc.split('\n'):
        line = line.strip()
        # don't take empty line
        if len(line) < 1:
            continue
        # ref mark
        if line.startswith('.. _'):
            continue
        return line

for package in packages:
    summary = extract_summary_line(sys.modules[package].__doc__)
    if summary is None or summary == '':
        summary = 'NO DOCUMENTATION (package %s)' % package
    t = template.replace('$SUMMARY', summary)
    t = t.replace('$PACKAGE', package)
    t = t.replace('$EXAMPLES_REF', '')
    t = t.replace('$EXAMPLES', '')

    # search packages
    for subpackage in packages:
        packagemodule = subpackage.rsplit('.', 1)[0]
        if packagemodule != package or len(subpackage.split('.')) <= 2:
            continue
        t += "    api-%s.rst\n" % subpackage

    # search modules
    m = list(modules.keys())
    
    m.sort(key=lambda x: extract_summary_line(sys.modules[x].__doc__).upper())
    for module in m:
        packagemodule = module.rsplit('.', 1)[0]
        if packagemodule != package:
            continue
        t += "    api-%s.rst\n" % module

    writefile('api-%s.rst' % package, t, dest_dir)


# Create index for all module
m = list(modules.keys())
m.sort()
refid = 0
for module in m:
    #summary = extract_summary_line(sys.modules[module].__doc__)
    #if summary is None or summary == '':
    #    summary = 'NO DOCUMENTATION (module %s)' % package

    # search examples
    example_output = []
    example_prefix = module
    if module.startswith('moho.'):
        example_prefix = module[5:]
    example_prefix = example_prefix.replace('.', '_')

    # try to found any example in framework directory
    list_examples = glob('%s*.py' % os.path.join(
        examples_framework_dir, example_prefix))
    for x in list_examples:
        # extract filename without directory
        xb = os.path.basename(x)

        # add a section !
        example_output.append('File :download:`%s <%s>` ::' % (
            xb, os.path.join('..', x)))

        # put the file in
        with open(x, 'r') as fd:
            d = fd.read().strip()
            d = '\t' + '\n\t'.join(d.split('\n'))
            example_output.append(d)

    #t = template.replace('$SUMMARY', summary)
    t = template_module
    t = t.replace('$PACKAGE', module)
    if len(example_output):
        refid += 1
        example_output = template_examples % (
            refid, '\n\n\n'.join(example_output))
        t = t.replace('$EXAMPLES_REF', template_examples_ref % refid)
        t = t.replace('$EXAMPLES', example_output)
    else:
        t = t.replace('$EXAMPLES_REF', '')
        t = t.replace('$EXAMPLES', '')
    writefile('api-%s.rst' % module, t, dest_dir)


# Generation finished
print('Auto-generation finished')
