

var MohoWebSocket = function(alias){
      var url = ((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.host + "/ws/"+alias
      var conn = undefined;
      var callbacks = {};
      
      this.connect = function(){
          if (conn == undefined){
                conn = new WebSocket(url);
                // dispatch to the right handlers
                conn.onmessage = function(evt){
                	if (evt.data instanceof Blob) {
                		dispatch(null, 'binary', evt.data);
                	}
                	else{
                		var json = JSON.parse(evt.data);
                		console.log("got event "+ evt.data );
                		dispatch(json.element, json.event, json.data);
                	}
                };
                conn.onclose = function(){dispatch(null, 'close')}
                conn.onopen = function(){dispatch(null, 'open')}
          }
      }
      
      this.bind = function(event_name, callback){
        callbacks[event_name] = callbacks[event_name] || [];
        callbacks[event_name].push(callback);
        return this;// chainable
      };
      
      this.disconnect=function(){
          conn.close();  
      };
    
      this.send = function(element, event_name, event_data){
        var payload = JSON.stringify({event:event_name, element: element, data: event_data});
        console.log("sending event "+ payload );
        conn.send( payload ); // <= send JSON data to socket server
        return this;
      };
      
      this.remote_dispatch=function(element, event, args){
          this.send(element, event, {args: args})  
      };
    
      var dispatch = function(element_id, event_name, data){
    	  if (element_id){
    		  $('#'+element_id).trigger(event_name, data);
    	  }
    	  else{
    		  var chain = callbacks[event_name];
    	      if(typeof chain == 'undefined') return; // no callbacks for this event
    	      for(var i = 0; i < chain.length; i++){
    	          	chain[i](data)
    	      }  
    	  }
      }
      
};


window.moho = {};

    
moho.init = function(){




    this.main_socket=new MohoWebSocket('moho');
    
    var wk=this;
    
    $(window).on('beforeunload', function(){
        wk.main_socket.onclose = function () {};
        wk.main_socket.disconnect();
    });

    this.main_socket.bind('html', function(data){
        var id = data.id;
        if (!id){
            return;
        }
        
        var action = data.action;
        var content = data.content;
        if (content == undefined){
            return;
        }
        if (action == 'update'){
            $("#"+id).html(content);
        }
        else if (action == 'add'){
            $("#"+id).append(content);
        }
        else if (action == 'remove'){
            $("#"+id).remove();
        }
        else if (action == 'clear'){
            $("#"+id).empty();
        }
    });

    this.main_socket.bind('cls', function(data){
        var id = data.id;
        if (!id){
            return;
        }
        var cls = data.cls;
        $("#"+id).attr('class', cls!==null ? cls : '');
    });
    
    this.main_socket.bind('attr', function(data){
        var id = data.id;
        if (!id){
            return;
        }
        var attrname = data.attrname;
        var attrvalue = data.attrvalue;
        $("#"+id).attr(attrname, attrvalue!==null ? attrvalue : '');
    });
    
    this.main_socket.bind('attrbool', function(data){
        var id = data.id;
        if (!id){
            return;
        }
        
        var elem=document.getElementById(id);
        if (elem){
            var attrname = data.attrname;
            var attrvalue = data.attrvalue;
            if (attrvalue){
                elem.setAttribute(attrname, attrname);
                if (attrname=='checked'){
                    elem.checked=true;
                }
            }
            else{
                elem.removeAttribute(attrname);
                if (attrname=='checked'){
                    elem.checked=false;
                }
            }
        }
    });

    this.main_socket.bind('css', function(data){
        var id = data.id;
        if (!id){
            return;
        }
        var css = data.css;
        $("#"+id).attr('style', css!==null ? css : ''); 
    });
}
    
$(document).ready(function(){
    moho.init();
     $(function() {
        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parents('.accordion-item').addClass('show');
            $(this).children('.accordion-body').addClass('show');
        });
    
        $('.collapse').on('hidden.bs.collapse', function () {
            $(this).parents('.accordion-item').removeClass('show');
            $(this).children('.accordion-body').removeClass('show');
        });
    });
    
});
