import numpy as np
from scipy import signal as sg

from moho.core.logger import Logger
from moho.uix.bootstrap.app import BootstrapApp
from moho.uix.body import Body
from moho.uix.bootstrap.tabs import TabPanel
from moho.uix.bootstrap.button import Button
from moho.uix.bootstrap.mpl.figure import MplFigure
from moho.core.properties import BooleanProperty, BoundedNumericProperty,\
NumericProperty
from moho.core.clock import Clock


class CustomButton(Button):
    
    def __init__(self, **kwargs):
        super(CustomButton, self).__init__(**kwargs)
        self._numclicks = 0
    
    def on_remote_click(self, *args, **kwargs):
        self.text = "Clicked {0}".format(self._numclicks+1)
        self._numclicks += 1
        
        
class DangerButton(Button):
    
    def on_remote_click(self, *args, **kwargs):
        st = self.btn_style
        if st == "outline-danger":
            self.btn_style = "primary"
            self.text = "No danger"
            self.icon = ""
        else:
            self.btn_style = "outline-danger"
            self.text = "Danger!" 
            self.icon = "ban"  



class ProgressTab(TabPanel):
    
    def progress_all_bars(self):
        staticval = self.progress_static.value
        stripedval = self.progress_striped.value
        animatedval = self.progress_animated.value
        
        self.progress_static.value +=5 if staticval<100 else 0
        self.progress_striped.value +=10 if stripedval<100 else 0
        self.progress_animated.value +=15 if animatedval<100 else 0
        
        if 0 <= staticval < 10:
            self.progress_static.type = "success"
        elif 10 <= staticval < 50:
            self.progress_static.type = "info"
        elif 50 <= staticval < 80:
            self.progress_static.type = "warning"
        elif 80 <= staticval <= 100:
            self.progress_static.type = "danger"
        else:
            self.progress_static.type = "default"
            
            
        if 0 <= stripedval < 10:
            self.progress_striped.type = "success"
        elif 10 <= stripedval < 50:
            self.progress_striped.type = "info"
        elif 50 <= stripedval < 80:
            self.progress_striped.type = "warning"
        elif 80 <= stripedval <= 100:
            self.progress_striped.type = "danger"
        else:
            self.progress_striped.type = "default"
            
        
        if 0 <= animatedval < 10:
            self.progress_animated.type = "success"
        elif 10 <= animatedval < 50:
            self.progress_animated.type = "info"
        elif 50 <= animatedval < 80:
            self.progress_animated.type = "warning"
        elif 80 <= animatedval <= 100:
            self.progress_animated.type = "danger"
        else:
            self.progress_animated.type = "default"
            
            
    def reset_all_bars(self):
        
        self.progress_static.value = 0
        self.progress_striped.value = 0
        self.progress_animated.value = 0
        
        self.progress_static.type = "default"
        self.progress_striped.type = "default"
        self.progress_animated.type = "default" 
        


class CustomMplFigure(MplFigure):
    
    live = BooleanProperty(False)
    
    time = BoundedNumericProperty(0.2, min = 0.0, max = 1)
    
    freq = NumericProperty(2)
    
    def __init__(self, *args, **kwargs):
        self._event = None
        tm = np.linspace(0, 2, 500)
        self.sin_vals = np.sin(2*np.pi*self.freq*tm)
        self.square_vals = sg.square(2*np.pi*self.freq*tm, duty=0.3)
        self.signal_time = tm
        self.map_x, self.map_y = np.meshgrid(np.linspace(0, 200, 500), np.linspace(0, 200, 500))
        
        super(CustomMplFigure, self).__init__(*args, **kwargs)
    
    def activate(self):
        if self._event is not None:
            Clock.unschedule(self._event)
            self._event = None
        self._event = Clock.schedule_interval(self.update_plots, self.time)
        self.live = True
        
    def stop(self):
        if self._event is not None:
            Clock.unschedule(self._event)
            self._event = None
        self.live = False
    
    def toggle(self):
        if self.live:
            self.stop()
        else:
            self.activate()
    
    def on_time(self, obj, val):
        if self.live:
            self.stop()
            self.activate()
        
    def update_plots(self, dt):
        delta_sine = np.random.random()*0.1
        delta_sq = np.random.random()*0.1
        cl = self.canvas_layout
        sbl = cl.subplots[0]
        sine_line = sbl.lines[0]
        sine_line.x = self.signal_time+delta_sine
        sine_line.y = self.sin_vals+np.random.random(size=len(self.sin_vals))*0.1
        
        sq_line = cl.subplots[1].lines[0]
        sq_line.x = self.signal_time+delta_sq
        sq_line.y = self.square_vals+np.random.random(size=len(self.sin_vals))*0.05
        
        x0 = 99+np.random.randint(-2, 2)
        y0 = 99+np.random.randint(-2, 2)
        sigma = np.random.random()*5
        
        map3d = cl.subplots[2]
        map3d.z = np.exp(-(x0-self.map_x)**2/(2*sigma**2))*np.exp(-(y0-self.map_y)**2/(2*sigma**2))
        
        self.update_figure()
        
        
        
        



class MainBody(Body):
    pass


class ShowApp(BootstrapApp):

    def build(self):
        return MainBody()


if __name__ == "__main__":

    Logger.setLevel('DEBUG')

    ShowApp(
        #        auth_controller=SimpleAuthenticationController(),
        cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
    ).run()
