Moho
====

Moho is an open source software framework for the rapid development of Rich Internet Applications written in Python.
Moho framework development was inspired by Kivy project .
We are using slightly modified versions of Kivy modules for Moho event-based model and moho language for designing.

If you are familiar with Kivy framework you will notice that design of Moho applications is very similar
to Kivy with the only difference, instead of running an OpenGL based desktop application you run a server-client application.

Moho is written in Python and Cython, with Tornado running a server application and Bootstrap as a base for
the frontend library with rich variety of widgets. 

Moho allows to create cross-platform applications which can be installed locally or on the server site for remote usage. 

Below are few screenshots of /examples/show application demonstrating major capabilities of Moho project.

<img src="doc/_static/images/scr1.png"  width="220" >
<img src="doc/_static/images/scr2.png"  width="220" >
<img src="doc/_static/images/scr3.png"  width="220" >
<img src="doc/_static/images/scr4.png"  width="220" >
<img src="doc/_static/images/scr5.png"  width="220" >
<img src="doc/_static/images/scr6.png"  width="220" >

For more information check https://escape-projects.gitlab.io/moho-framework/

If you are familiar with Docker use the following command to probe the "Showcase" example:

    docker run -d -p 8888:8888 escapeapp/moho-examples:master
