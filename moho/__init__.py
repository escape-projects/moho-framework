'''
Moho framework
==============
'''

__all__ = (
    'require',
    'moho_base_dir',
    'moho_data_dir', 
    'moho_icons_dir', 'moho_home_dir',
    'moho_config_fn', 'moho_usermodules_dir',
)

import sys
import shutil
from getopt import getopt, GetoptError
from os import environ, mkdir
from os.path import dirname, join, basename, exists, expanduser
import pkgutil
from moho.core.compat import PY2
from moho.core.logger import Logger, LOG_LEVELS
from moho.core.utils import platform

MAJOR = 0
MINOR = 1
MICRO = 6
RELEASE = False

__version__ = '%d.%d.%d' % (MAJOR, MINOR, MICRO)

if not RELEASE and '.dev0' not in __version__:
    __version__ += '.dev0'

try:
    from moho.core.version import __hash__, __date__
    __hash__ = __hash__[:7]
except ImportError:
    __hash__ = __date__ = ''

# internals for post-configuration
__moho_post_configuration = []


if platform == 'macosx' and sys.maxsize < 9223372036854775807:
    r = '''Unsupported Python version detected!:
    Moho requires a 64 bit version of Python to run on OS X. We strongly
    advise you to use the version of Python that is provided by Apple
    (don't use ports, fink or homebrew unless you know what you're
    doing).
    '''
    Logger.critical(r)


def require(version):
    '''Require can be used to check the minimum version required to run a Moho
    application. For example, you can start your application code like this::

        import moho
        moho.require('1.0.1')

    If a user attempts to run your application with a version of Moho that is
    older than the specified version, an Exception is raised.

    The Moho version string is built like this::

        X.Y.Z[-tag[-tagrevision]]

        X is the major version
        Y is the minor version
        Z is the bugfixes revision

    The tag is optional, but may be one of 'dev', 'alpha', or 'beta'.
    The tagrevision is the revision of the tag.

    .. warning::

        You must not ask for a version with a tag, except -dev. Asking for a
        'dev' version will just warn the user if the current Moho
        version is not a -dev, but it will never raise an exception.
        You must not ask for a version with a tagrevision.

    '''

    def parse_version(version):
        # check for tag
        tag = None
        tagrev = None
        if '-' in version:
            v = version.split('-')
            if len(v) == 2:
                version, tag = v
            elif len(v) == 3:
                version, tag, tagrev = v
            else:
                raise Exception('Revision format must be X.Y.Z[-tag]')

        # check x y z
        v = version.split('.')
        if len(v) != 3:
            if 'dev0' in v:
                tag = v.pop()
            else:
                raise Exception('Revision format must be X.Y.Z[-tag]')
        return [int(x) for x in v], tag, tagrev

    # user version
    revision, tag, tagrev = parse_version(version)
    # current version
    sysrevision, systag, _ = parse_version(__version__)

    # ensure that the required version don't contain tag, except dev
    if tag not in (None, 'dev'):
        raise Exception('Revision format must not have any tag except "dev"')
    if tag == 'dev' and systag != 'dev':
        Logger.warning('Application requested a -dev version of Moho. '
                       '(You have %s, but the application requires %s)' % (
                           __version__, version))
    # not tag rev (-alpha-1, -beta-x) allowed.
    if tagrev is not None:
        raise Exception('Revision format must not contain any tagrevision')

    # finally, checking revision
    if sysrevision < revision:
        raise Exception('The version of Moho installed on this system '
                        'is too old. '
                        '(You have %s, but the application requires %s)' % (
                            __version__, version))

# Extract all needed path in moho
#: Moho directory
moho_base_dir = dirname(sys.modules[__name__].__file__)
#: Moho modules directory

#: Moho data directory
moho_data_dir = environ.get('MOHO_DATA_DIR',
                            join(moho_base_dir, 'data'))
#: Moho binary deps directory
moho_icons_dir = join(moho_data_dir, 'icons', '')
#: Moho user-home storage directory
moho_home_dir = ''
#: Moho configuration filename
moho_config_fn = ''
#: Moho user modules directory
moho_usermodules_dir = ''
