"""
Registry
========



"""

from weakref import WeakValueDictionary


element_registry = WeakValueDictionary()
'''Container for created elements.'''