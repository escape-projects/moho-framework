"""
Handlers
========
"""
import json

from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler
from moho.core.registry import element_registry

from moho.core.logger import Logger
from tornado.escape import xhtml_escape


USER_COOKIE_NAME = 'moho_user'


def get_current_user(handler):
    if not handler.application.auth_enabled:
        return "<anonymous>"
    user = handler.get_secure_cookie(USER_COOKIE_NAME)
    if user is not None:
        user = user.decode('utf-8')
        if handler.application.authorize(user):
            return user
    return None


class MainHandler(RequestHandler):

    def get_current_user(self):
        return get_current_user(self)

    def get(self):
        app = self.application
        if self.current_user:
            self.render('index.html', app=app, root=app.root)
            return
        self.render('index.html', app=app, root=app.login_root)

    def post(self):
        action = xhtml_escape(self.get_argument("action"))
        if action == "login":
            getusername = xhtml_escape(self.get_argument("username"))
            getpassword = xhtml_escape(self.get_argument("password"))
            if not self.application.authenticate(getusername, getpassword):
                Logger.error("Authentication failed")
                self.write(json.dumps({"error": "Authentication failed"}))
                return
            self.set_secure_cookie(USER_COOKIE_NAME, getusername)
        elif action == "logout":
            self.clear_cookie(USER_COOKIE_NAME)


class MohoWebSocketHandler(WebSocketHandler):

    alias = ''

    def get_current_user(self):
        return get_current_user(self)

    def initialize(self):
        if not self.alias:
            raise ValueError("empty alias")
        WebSocketHandler.initialize(self)

    def open(self):
        if not self.current_user:
            Logger.info("Authentication failed")
            self.close(reason="Authentication failed")
        if not self.alias:
            raise TypeError("")
        Logger.info("Connected websocket: {}".format(self.alias))
        cl = self.application._clients.get(self.alias)
        if cl is None:
            self.application._clients[self.alias] = [self]
        else:
            self.application._clients[self.alias].append(self)

    def on_close(self):
        Logger.info("Disconnected websocket: {}".format(self.alias))
        cl = self.application._clients.get(self.alias)
        if cl is not None and self in cl:
            cl.remove(self)

    def on_message(self, message):
        user = self.current_user
        if not user:
            Logger.info("Authentication failed")
            self.close(reason="Authentication failed")
        info = json.loads(message)
        elid = info.get('element', None)
        event = info['event']
        data = info.get('data', None)
        # very dirty must be changed
        if elid is None:
            element = self.application.root
        else:
            element = element_registry.get(int(elid[2:]), None)
        if element is None:
            raise ValueError("element with id {} was not found".format(elid))
        kwargs = data.get('args') or {}
        if element.is_remote_event_type(event):
            element.dispatch(event, user=user, **kwargs)
        else:
            raise TypeError(
                "event {0} is not registered as remote".format(event))

    def remote_dispatch(self, element, event, **kwargs):
        info = {
            'event': event,
            'element': element.tagid if element is not None else None,
            'data': kwargs
        }
        self.write_message(info)

    def remote_binary(self, data):
        self.write_message(data, binary=True)


class MainWebSocketHandler(MohoWebSocketHandler):

    alias = 'moho'

    def update_element_content(self, element, content):
        self.remote_dispatch(
            None, 'html', id=element.tagid, content=content, action="update")

    def add_element_content(self, element, content):
        self.remote_dispatch(
            None, 'html', id=element.tagid, content=content, action="add")

    def remove_element_content(self, element, content):
        self.remote_dispatch(None, 'html', id=element.tagid, action="clear")

    def remove_element(self, element):
        self.remote_dispatch(None, 'html', id=element.tagid, action="remove")

    def update_element_class(self, element, cls):
        self.remote_dispatch(None, 'cls', cls=cls, id=element.tagid, )

    def update_element_css(self, element, css):
        self.remote_dispatch(None, 'css', id=element.tagid, css=css)

    def update_element_attr(self, element, attrname, attrvalue):
        self.remote_dispatch(
            None, 'attr', id=element.tagid,
            attrname=attrname, attrvalue=attrvalue)

    def update_boolean_attr(self, element, attrname, attrvalue):
        self.remote_dispatch(
            None, 'attrbool', id=element.tagid,
            attrname=attrname, attrvalue=attrvalue)
