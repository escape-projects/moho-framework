"""
IOLoop
======
"""
from moho.core.clock import Clock
from tornado.ioloop import IOLoop


class WebAppIOLoop(IOLoop.configurable_default()):

    def tick(self):
        # Moho clock tick
        Clock.tick()
        self.add_callback(self.tick)

    def start(self):
        self.add_callback(self.tick)
        super(WebAppIOLoop, self).start()
