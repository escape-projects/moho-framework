# This is a "jumping" module, required for python-for-android project
# Because we are putting all the module into the same .so, there can be name
# conflict. We have one conflict with pygame.event and moho.event => Both are
# python extension and have the same "initevent" symbol. So right now, just
# rename this one.
__all__ = ('EventDispatcher', 'ObjectWithUid', 'Observable')

import moho.core._event
__doc__ = moho.core._event.__doc__
EventDispatcher = moho.core._event.EventDispatcher
ObjectWithUid = moho.core._event.ObjectWithUid
Observable = moho.core._event.Observable
