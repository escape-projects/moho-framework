from moho.core._event cimport EventDispatcher
from moho.core.properties cimport (
    Property, NumericProperty, BoundedNumericProperty,
    BooleanProperty, VariableListProperty, ListProperty)


cdef class _Attribute(Property):
    cdef str _attrtype
    cdef int _remote
    cdef list options
    cdef dict values
    cdef str _attrname
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _NumericAttribute(NumericProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cdef list options
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _BoundedNumericAttribute(BoundedNumericProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cdef list options
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _BooleanAttribute(BooleanProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _LogicalAttribute(BooleanProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _VariableListAttribute(VariableListProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cdef list _options
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _ListAttribute(ListProperty):
    cdef str _attrtype
    cdef int _remote
    cdef object _length
    cdef str _attrname
    cdef list _options
    cpdef attrvalue(self, EventDispatcher obj)


cdef class _ColorAttribute(Property):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cdef str _format
    cdef list _options
    cpdef attrvalue(self, EventDispatcher obj)
    cdef list parse_str(self, EventDispatcher obj, value)


cdef class _GenericEventAttribute(BooleanProperty):
    cdef str _attrtype
    cdef int _remote
    cdef str _attrname
    cdef object _event_handler
    cdef object _prevent_default
    cdef object _remdispatch
    cdef object _event_args
    cdef str _event_js
    cpdef attrvalue(self, EventDispatcher obj)

