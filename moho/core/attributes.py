'''
Html Attributes
================

In HTML langauge attributes are always specified in the start tag and
provide additional information about HTML elements. Usually HTML attributes come in name/pare
pairs like: name="value". But there also exceptions, like `checked`.

At the backend side each HtmlElement object can have attributes `class Attribute`, 
which behave mainly like properties `class Property`, but also can dispatch their value change to the frontend. 
When value of an attribute is changed and it is set as `remote`, the backend, i.e.
server, sends the corresponding event to all clients.

Most of the attributes accept `None` values. In this case no value is sent to the frontend.

The html attributes support several types and behaves different depending on their type:

1. `element` - attributes, which appear on the level of html element, like `width`, `height`, `href`, `style`
2. `class` - normally boolean or string attributes. All attributes of this type which belong to the same
   element are accumulated and appear as `class` attribute in generated html code
3. `style` - All attributes of this type which belong to the same element are accumulated and appear as `style` attribute in generated html code


'''



from tornado.template import Template
from tornado.escape import native_str
import string




from moho.core._attributes import (
    _Attribute, _NumericAttribute,
    _BoundedNumericAttribute,
    _BooleanAttribute,
    _VariableListAttribute,
    _ColorAttribute, _GenericEventAttribute,
    _LogicalAttribute, _ListAttribute)


class ElementAttribute(_Attribute):
    '''Element attribute class with a string value.

    :Parameters:
        `defaultvalue`: string, defaults to ''
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
             Other possible values which are accepted by the attribute instance.
             For example some numeric attributes can accept also string values like `inherit`.
        `values`: list of values relevant for options
             This list contain actual values which are sent to frontend when
             the property value is set to one of optional values. If `values` list is not set, it is equal to `options`.
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementAttribute, self).__init__(*largs, **kwargs)


class ElementNumericAttribute(_NumericAttribute):
    '''Element attribute class with a numeric value.

    :Parameters:
        `defaultvalue`: numeric, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
    '''

    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementNumericAttribute, self).__init__(*largs, **kwargs)


class ElementBoundedNumericAttribute(_BoundedNumericAttribute):
    '''Element attribute class with a bounded numeric value.

    :Parameters:
        `defaultvalue`: numeric, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
        `min`: float
            Lower boundary
        `max`: float
            Upper boundary
    '''

    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementBoundedNumericAttribute, self).__init__(*largs, **kwargs)


class ElementBooleanAttribute(_BooleanAttribute):
    '''Element attribute class with a boolean value. This attribute dispatches its name 
    when it is true and empty string when it is false. It is useful for the atributes without name=value pare, 
    like `checked`.
    
    :Parameters:
        `defaultvalue`: true, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''    
    
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementBooleanAttribute, self).__init__(*largs, **kwargs)


class ElementLogicalAttribute(_LogicalAttribute):
    '''Element attribute class with a boolean value. In opposite to the ElementBooleanAttribute, 
    this attribute dispatches its name and value.
    
    :Parameters:
        `defaultvalue`: true, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementLogicalAttribute, self).__init__(*largs, **kwargs)


class ElementColorAttribute(_ColorAttribute):
    '''Element attribute class with color `rgb` or `rgba` values. 

    :Parameters:
        `defaultvalue`: None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
        `format`: string, `css` or empty string
            When format is `css`, 'rgb' or 'rgba' prefix is added o the returned attribute value. 
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementColorAttribute, self).__init__(*largs, **kwargs)


class ElementListAttribute(_ListAttribute):
    '''Element attribute class for a list of values. 

    :Parameters:
        `defaultvalue`: None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''

    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementListAttribute, self).__init__(*largs, **kwargs)


class ElementVariableListAttribute(_VariableListAttribute):
    '''The Element list attribute class that allows you to work with a variable number of
    list items and to expand them to the desired list size.

    :Parameters:
        `defaultvalue`: list, defaults to []
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''

    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'element'
        super(ElementVariableListAttribute, self).__init__(*largs, **kwargs)
        
        
class ClassAttribute(_Attribute):
    '''Attribute with a class name. Use it when you would like to add certain style class to the element. 
    Example:
    
    >>> alert_style = ClassAttribute("primary", options=[
        "primary", "secondary", "success", "info",
        "warning", "danger", "default"],
        values=["alert-primary", "alert-secondary", "alert-success", "alert-info",
                "alert-warning", "alert-danger", "alert-default"])

    When `alert_style` is set to one of values listed in 'options' it will dispatch the corresponding value
    with 'alert-' prefix. This class name will be listed under the `class=` attribute.
    
    :Parameters:
        `defaultvalue`: None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
        `values`: list of values
            This list contain actual values which are sent to frontend when
            the property value is set to one of optional values. If `values` list is not set, it is equal to `options`
    '''

    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'class'
        super(ClassAttribute, self).__init__(*largs, **kwargs)


class ClassBooleanAttribute(_BooleanAttribute):
    '''ClassAttribute with boolean behavior. When it is true, returns its name as the attribute value.
        
    :Parameters:
        `defaultvalue`: false
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'class'
        super(ClassBooleanAttribute, self).__init__(*largs, **kwargs)


class StyleAttribute(_Attribute):
    '''Style attribute class with a string value.

    :Parameters:
        `defaultvalue`: string, defaults to ''
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
        `values`: list of values
            This list contain actual values which are sent to frontend when
            the property value is set to one of optional values. If `values` list is not set, it is equal to `options`
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'style'
        super(StyleAttribute, self).__init__(*largs, **kwargs)


class StyleNumericAttribute(_NumericAttribute):
    '''Style attribute class with a numeric value.

    :Parameters:
        `defaultvalue`: numeric, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
    '''
    
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'style'
        super(StyleNumericAttribute, self).__init__(*largs, **kwargs)


class StyleVariableListAttribute(_VariableListAttribute):
    '''A style ListAttribute that allows you to work with a variable number of
    list items and to expand them to the desired list size.

    For example, style attributes like `margin` or `padding` can accept one numeric value
    which was applied equally to the left, top, right and bottom.
    Now padding or margin can be given one, two or four values, which are
    expanded into a length four list [left, top, right, bottom] and stored
    in the attribute.

    :Parameters:
        `defaultvalue`: list, defaults to []
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'style'
        super(StyleVariableListAttribute, self).__init__(*largs, **kwargs)



class StyleColorAttribute(_ColorAttribute):
    '''A style ListAttribute with 3 or 4 elements with RGB or RGBA color values.

    :Parameters:
        `defaultvalue`: list, defaults to None
            Specifies the default value of the attribute.
        `remote`: boolean
            This parameter is relevant for element attributes only, class and style attributes
            are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings
            Other possible values which are accepted by the attribute instance.
            For example some numeric attributes can accept also string values like `inherit`
    '''
    def __init__(self, *largs, **kwargs):
        kwargs['attrtype'] = 'style'
        kwargs['format'] = 'css'
        super(StyleColorAttribute, self).__init__(*largs, **kwargs)


DEFAULT_EVENT_TEMPLATE = string.Template("""
{% if prevent_default %}event.preventDefault();{% end %}
$code
{% if dispatch %}
moho.main_socket.remote_dispatch(
'{{obj.tagid}}', '{{event_slot}}',{ 
{% for k in event_args %} {{ k }}: {{ event_args[k] }}, {% end %} });
{% end %}
""")


def default_event_handler(
        obj, event_slot, prevent_default, event_args, event_js, dispatch):
    tpl = DEFAULT_EVENT_TEMPLATE.substitute(code=event_js)
    return native_str(Template(tpl).generate(
        obj=obj, event_slot=event_slot,
        prevent_default=prevent_default,
        event_args=event_args, dispatch=dispatch))


class EventAttribute(_GenericEventAttribute):
    '''The event attribute class for HTML event attributes like `onclick`.
    Event attributes can be activated/deactivated  by setting them to True/False.

    :Parameters:
        `defaultvalue`: string, defaults to ''
            Specifies the default value of the attribute.
        `remdispatch`: boolean
            If True, prevents dispatching of the remote event
        `event_handler`: callable, `default_event_handler` by default
            The callable object which when called returns as the result a javascript code, 
            which will be executed on the client side when this event occurs.
        `event_args`: dictionary
             Event arguments to be sent to the backend.
        'event_js': string
             Javascript code to be called when event is activated
    '''
    def __init__(self, defaultvalue=False,
                 eventhandler=default_event_handler, **kwargs):
        kwargs['attrtype'] = 'element'
        super(EventAttribute, self).__init__(
            defaultvalue, eventhandler, **kwargs)
