"""
Authentication Controllers
==========================
"""

class BaseAuthController(object):

    def authenticate(self, *args, **kwargs):
        raise NotImplementedError

    def authorize(self, *args, **kwargs):
        raise NotImplementedError

    def deauthorize(self, *args, **kwargs):
        raise NotImplementedError


class SimpleAuthenticationController(BaseAuthController):

    users = {
        'user1': '1234',
        'user2': '5678'
    }

    authorized_users = []

    def authenticate(self, user, passwd):
        if user in self.users:
            if self.users[user] == passwd:
                self.authorized_users.append(user)
                return True
            return False
        return False

    def authorize(self, user):
        return user in self.authorized_users

    def deauthorize(self, user):
        self.authorized_users.remove(user)
