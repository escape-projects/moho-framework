'''
Application classes
==========================

This module contains the WebApp base class for applications and registers the core classes to the Factory.
Each frontend has its own application class implementation, such as `moho.uix.bootstrap.app.BootstrapApp`.
This implementation customizes some settings like stylesheets and registers additional classes to the Factory.

The WebApp class or its customized version is necessary to create applications. 

The minimum application code in `app.py` looks like this:

.. code-block:: python


    class MainBody(Body):
        pass
    
    
    class ShowApp(BootstrapApp):
    
        def build(self):
            return MainBody()
    
    
    if __name__ == "__main__":
    
        Logger.setLevel('DEBUG')
    
        ShowApp(
            #        auth_controller=SimpleAuthenticationController(),
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        ).run()


'''



__all__ = ('WebApp', )

import os
from inspect import getfile
from os.path import dirname, join, exists, expanduser, isfile
from tornado.web import Application, url
from moho.core.auth import BaseAuthController
from moho.core.compat import iteritems
from moho.core.event import EventDispatcher
from moho.core.factory import Factory
from moho.core.handlers import MainWebSocketHandler, MainHandler
from moho.core.ioloop import WebAppIOLoop as IOLoop
from moho.core.logger import Logger
from moho.core.properties import (ObjectProperty, StringProperty, BooleanProperty,
                                  ListProperty, NumericProperty, AliasProperty)
from moho.core.registry import element_registry
from moho.core.resources import resource_find
from moho.core.utils import platform
from moho.lang import Builder


r = Factory.register

# registering classes in the factory

r('Cache', module='moho.core.cache')
r('ClockBase', module='moho.core.clock')
r('EventDispatcher', module='moho.core.event')
r('Observable', module='moho.core.event')
r('FactoryException', module='moho.core.factory')
r('Parser', module='moho.lang.parser')
r('LoggerHistory', module='moho.core.logger')
r('NumericProperty', module='moho.core.properties')
r('StringProperty', module='moho.core.properties')
r('ListProperty', module='moho.core.properties')
r('ObjectProperty', module='moho.core.properties')
r('BooleanProperty', module='moho.core.properties')
r('BoundedNumericProperty', module='moho.core.properties')
r('OptionProperty', module='moho.core.properties')
r('ReferenceListProperty', module='moho.core.properties')
r('AliasProperty', module='moho.core.properties')
r('NumericProperty', module='moho.core.properties')
r('DictProperty', module='moho.core.properties')
r('VariableListProperty', module='moho.core.properties')
r('Property', module='moho.core.properties')

# registering classes in the factory
r('Element', module='moho.uix.element')
r('HtmlElement', module='moho.uix.element')


class WebApp(EventDispatcher, Application):
    ''' Application class, see module documentation for more information.

    :Events:
        `on_start`:
            Fired when the application is being started (before the
            :func:`~moho.core.base.runTouchApp` call.
        `on_stop`:
            Fired when the application stops.
    '''

    title = StringProperty('Moho')

    autoreload = BooleanProperty(True)

    debug = BooleanProperty(True)

    default_handler_class = ObjectProperty(None)

    default_handler_args = ListProperty([])

    compress_response = BooleanProperty(True)

    gzip = BooleanProperty(True)

    serve_traceback = BooleanProperty(True)

    websocket_ping_interval = NumericProperty(0)

    'Authentication controller class, No authentication if None'
    auth_controller = ObjectProperty(None, baseclass=BaseAuthController)

    cookie_secret = StringProperty('')

    compiled_template_cache = BooleanProperty(False)

    template_path = StringProperty(os.path.abspath('./data/templates'))

    static_path = StringProperty(
        os.path.join(os.path.abspath('./data/static')))

    mh_directory = StringProperty(None)
    '''Path of the directory where application mh file is stored, defaults to None

    If a mh_directory is set, it will be used to get the initial mh file. By
    default, the file is assumed to be in the same directory as the current App
    definition file.
    '''

    mh_file = StringProperty(None)
    '''Filename of the Mh file to load, defaults to None.

    If a mh_file is set, it will be loaded when the application starts. The
    loading of the "default" mh file will be prevented.
    '''

    root = ObjectProperty(None)

    login_root = ObjectProperty(None)

    stylesheets = ListProperty(
        ['static/css/styles.css']
    )

    scripts = ListProperty(
        ['static/js/jquery.min.js', 'static/js/moho.js']
    )

    ioloop = ObjectProperty(None)

    def get_css(self):
        if self.root is None:
            return ''
        stylelist = [self.root.css]
        for el in element_registry.values():
            stylelist.append(el.css)
        return "".join(stylelist)

    css = AliasProperty(get_css, None)

    handlers_list = ListProperty([url(r"/", MainHandler, name="main"),
                                  url(r"/ws/moho", MainWebSocketHandler, name="websocket")])

    # Return the current running App instance
    _running_app = None

    __events__ = ('on_start', 'on_stop')

    def __init__(self, **kwargs):
        WebApp._running_app = self
        if WebApp.ioloop is not None:
            Logger.info("No active IOLoop found, starting new...")
            WebApp.ioloop = IOLoop().make_current()
        self._clients = {}
        self._app_directory = None
        self._app_name = None
        self._app_settings = None
        self._app_window = None
        EventDispatcher.__init__(self, **kwargs)
        Application.__init__(self, handlers=self.handlers_list,
                             autoreload=self.autoreload,
                             debug=self.debug,
                             default_handler_class=self.default_handler_class,
                             default_handler_args=self.default_handler_args,
                             compress_response=self.compress_response,
                             gzip=self.gzip,
                             serve_traceback=self.serve_traceback,
                             websocket_ping_interval=self.websocket_ping_interval,
                             cookie_secret=self.cookie_secret,
                             compiled_template_cache=self.compiled_template_cache,
                             template_path=self.template_path,
                             static_path=self.static_path
                             )
        self.built = False

        #: Options passed to the __init__ of the App
        self.options = kwargs

    def build(self):
        '''Initializes the application; it will be called only once.
        If this method returns a element (tree), it will be used as the root
        element and added to the window.

        :return:
            None or a root :class:`~moho.uix.element.HtmlElement` instance
            if no self.root exists.'''

        raise NotImplementedError()

    def build_login(self):
        raise NotImplementedError()

    def load_mh(self, filename=None):
        '''This method is invoked the first time the app is being run if no
        element tree has been constructed before for this app.
        This method then looks for a matching kv file in the same directory as
        the file that contains the application class.

        For example, say you have a file named main.py that contains::

            class ShowcaseApp(App):
                pass

        This method will search for a file named `showcase.mh` in
        the directory that contains main.py. The name of the mh file has to be
        the lowercase name of the class, without the 'App' postfix at the end
        if it exists.

        You can define rules and a root element in your mh file::

            <ClassName>: # this is a rule
                ...

            ClassName: # this is a root element
                ...

        There must be only one root element. See the :doc:`api-moho.lang`
        documentation for more information on how to create mh files. If your
        mh file contains a root element, it will be used as self.root, the root
        element for the application.

        .. note::

            This function is called from :meth:`run`, therefore, any element
            whose styling is defined in this mh file and is created before
            :meth:`run` is called (e.g. in `__init__`), won't have its styling
            applied. Note that :meth:`build` is called after :attr:`load_mh`
            has been called.
        '''
        # Detect filename automatically if it was not specified.
        if filename:
            filename = resource_find(filename)
        else:
            try:
                default_mh_directory = dirname(getfile(self.__class__))
                if default_mh_directory == '':
                    default_mh_directory = '.'
            except TypeError:
                # if it's a builtin module.. use the current dir.
                default_mh_directory = '.'

            mh_directory = self.mh_directory or default_mh_directory
            clsname = self.__class__.__name__.lower()
            if (clsname.endswith('app') and
                    not isfile(join(mh_directory, '%s.mh' % clsname))):
                clsname = clsname[:-3]
            filename = join(mh_directory, '%s.mh' % clsname)

        # Load mh file
        Logger.debug('App: Loading mh <{0}>'.format(filename))
        rfilename = resource_find(filename)
        if rfilename is None or not exists(rfilename):
            Logger.debug('App: mh <%s> not found' % filename)
            return False
        root = Builder.load_file(rfilename)
        if root:
            self.root = root
        return True

    def get_application_name(self):
        '''Return the name of the application.
        '''
        if self.title is not None:
            return self.title
        clsname = self.__class__.__name__
        if clsname.endswith('App'):
            clsname = clsname[:-3]
        return clsname

    @property
    def directory(self):
        '''
        Return the directory where the application lives.
        '''
        if self._app_directory is None:
            try:
                self._app_directory = dirname(getfile(self.__class__))
                if self._app_directory == '':
                    self._app_directory = '.'
            except TypeError:
                # if it's a builtin module.. use the current dir.
                self._app_directory = '.'
        return self._app_directory

    @property
    def user_data_dir(self):
        '''
        Returns the path to the directory in the users file system which the
        application can use to store additional data.

        Different platforms have different conventions with regards to where
        the user can store data such as preferences, saved games and settings.
        This function implements these conventions. The <app_name> directory
        is created when the property is called, unless it already exists.

        On Windows, `%APPDATA%/<app_name>` is returned.

        On OS X, `~/Library/Application Support/<app_name>` is returned.

        On Linux, `$XDG_CONFIG_HOME/<app_name>` is returned.
        '''
        data_dir = ""
        if platform == 'ios':
            data_dir = join('~/Documents', self.name)
        elif platform == 'android':
            data_dir = join('/sdcard', self.name)
        elif platform == 'win':
            data_dir = os.path.join(os.environ['APPDATA'], self.name)
        elif platform == 'macosx':
            data_dir = '~/Library/Application Support/{}'.format(self.name)
        else:  # _platform == 'linux' or anything else...:
            data_dir = os.environ.get('XDG_CONFIG_HOME', '~/.config')
            data_dir = join(data_dir, self.name)
        data_dir = expanduser(data_dir)
        if not exists(data_dir):
            os.makedirs(data_dir)
        return data_dir

    @property
    def name(self):
        '''
        Return the name of the application based on the class name.
        '''
        if self._app_name is None:
            clsname = self.__class__.__name__
            if clsname.endswith('App'):
                clsname = clsname[:-3]
            self._app_name = clsname.lower()
        return self._app_name

    def bind_elements(self, element_registry):
        self.root.bind_to_app()
        for _, el in iteritems(element_registry):
            el.bind_to_app()

    def unbind_elements(self, element_registry):
        self.root.unbind_from_app()
        for _, el in iteritems(element_registry):
            el.unbind_from_app()

    def _initialize_attributes(self, element_registry):
        self.root._initialize_attributes()
        for _, el in iteritems(element_registry):
            el._initialize_attributes()

    def run(self, port=8888, address="", browser=True):
        '''Launches the app in standalone mode.
        '''
        from moho.uix.element import HtmlElement
        if not self.built:
            from moho.uix.body import Body
            self.load_mh(filename=self.mh_file)
            root = self.build()
            if not isinstance(root, Body):
                raise TypeError("root can be only of type Body")
            self.root = root

            if self.auth_enabled:
                login_root = self.build_login()
                if not isinstance(login_root, Body):
                    raise TypeError("root can be only of type Body")
                self.login_root = login_root

        self._initialize_attributes(element_registry)
        self.bind_elements(element_registry)

        if self.root:
            for el in element_registry.values():
                if isinstance(el, HtmlElement):
                    self._init_element_handlers(el)

        if self.login_root:
            for el in element_registry.values():
                if isinstance(el, HtmlElement):
                    self._init_element_handlers(el)

        self.dispatch('on_start')
        self.listen(port, address)
        
        
        try:
            IOLoop.current().start()
        except KeyboardInterrupt:
            Logger.warning("Interrupted by user...")
        Logger.warning("Stopping the application.")
        self.stop()

    def _init_element_handlers(self, el):
        wcls = el.websocket_cls
        whost = el.websocket_host
        stylesrc = el.stylesheet
        scriptsrc = el.script
        if wcls is not None:
            self.add_handlers(
                r".*", [(whost, wcls)])
        if stylesrc and stylesrc not in self.stylesheets:
            self.stylesheets.append(stylesrc)
        if scriptsrc and scriptsrc not in self.scripts:
            self.scripts.append(scriptsrc)

    def stop(self, *largs):
        '''Stop the application.

        If you use this method, the whole application will stop by issuing
        a call to :func:`~moho.core.base.stopTouchApp`.
        '''
        self.dispatch('on_stop')
        self.unbind_elements(element_registry)
        IOLoop.current().stop()

    def on_start(self):
        '''Event handler for the `on_start` event which is fired after
        initialization (after build() has been called) but before the
        application has started running.
        '''
        pass

    def on_stop(self):
        '''Event handler for the `on_stop` event which is fired when the
        application has finished running (i.e. the window is about to be
        closed).
        '''
        pass

    @staticmethod
    def get_running_app():
        '''Return the currently running application instance.
        '''
        return WebApp._running_app

    def update_element_content(self, element, content):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.update_element_content(element, content)

    def add_element_content(self, element, content):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.add_element_content(element, content)

    def remove_element_content(self, element):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.remove_element_content(element)

    def remove_element(self, element):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.remove_element(element)

    def update_element_css(self, element, css):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.update_element_css(element, css)

    def update_element_class(self, element, cls):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.update_element_class(element, cls)

    def update_element_attr(self, element, attrname, attrvalue):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.update_element_attr(element, attrname, attrvalue)

    def update_boolean_attr(self, element, attrname, attrvalue):
        for sock in self._clients.get(MainWebSocketHandler.alias, []):
            sock.update_boolean_attr(element, attrname, attrvalue)

    def remote_dispatch(self, wsalias, element, event, **kwargs):
        for sock in self._clients.get(wsalias, []):
            sock.remote_dispatch(element, event, **kwargs)

    def remote_binary(self, wsalias, data):
        for sock in self._clients.get(wsalias, []):
            sock.remote_binary(data)

    @property
    def auth_enabled(self):
        return self.auth_controller is not None

    def authenticate(self, *args, **kwargs):
        if self.auth_enabled:
            return self.auth_controller.authenticate(*args, **kwargs)
        return False

    def authorize(self, user):
        if self.auth_enabled:
            if self.auth_controller.authorize(user):
                return True
            return False
        return True

    def deauthorize(self, user):
        self.auth_controller.deauthorize(user)
