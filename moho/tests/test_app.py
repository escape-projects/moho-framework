import unittest
from moho.core.app import WebApp
from moho.core.ioloop import WebAppIOLoop
from moho.core.clock import Clock
import os.path


class TestWebApp(WebApp):
    def build(self):
        from moho.uix.body import Body
        return Body()


class AppTest(unittest.TestCase):
    def test_scheduled_stop(self):
        io = WebAppIOLoop()
        io.make_current()
        a = TestWebApp()
        Clock.schedule_once(a.stop, 0.1)
        a.run()

    def test_user_data_dir(self):
        a = TestWebApp()
        data_dir = a.user_data_dir
        if not os.path.exists(data_dir):
            raise Exception("user_data_dir doesn't exists")
