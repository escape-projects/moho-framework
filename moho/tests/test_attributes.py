'''
Test properties attached to a widget
'''

import unittest
from moho.core.event import EventDispatcher
from functools import partial


class TestAttribute(EventDispatcher):
    pass


wid = TestAttribute()


class AttributesTestCase(unittest.TestCase):

    def test_base(self):
        from moho.core._attributes import _Attribute as Attribute

        a = Attribute("-1")
        a.link(wid, 'a')
        a.link_deps(wid, 'a')
        self.assertEqual(a.get(wid), "-1")
        a.set(wid, "0")
        self.assertEqual(a.get(wid), "0")
        a.set(wid, "1")
        self.assertEqual(a.get(wid), "1")

    def test_observer(self):
        from moho.core._attributes import _Attribute as Attribute

        a = Attribute("-1")
        a.link(wid, 'a')
        a.link_deps(wid, 'a')
        self.assertEqual(a.get(wid), "-1")
        global observe_called
        observe_called = 0

        def observe(obj, value):
            global observe_called
            observe_called = 1
        a.bind(wid, observe)

        a.set(wid, "0")
        self.assertEqual(a.get(wid), "0")
        self.assertEqual(observe_called, 1)
        observe_called = 0
        a.set(wid, "0")
        self.assertEqual(a.get(wid), "0")
        self.assertEqual(observe_called, 0)
        a.set(wid, "1")
        self.assertEqual(a.get(wid), "1")
        self.assertEqual(observe_called, 1)

    def test_numericcheck(self):
        from moho.core._attributes import _NumericAttribute as NumericAttribute

        a = NumericAttribute()
        a.link(wid, 'a')
        a.link_deps(wid, 'a')
        self.assertEqual(a.get(wid), 0)
        a.set(wid, 99)
        self.assertEqual(a.get(wid), 99)
        
    def test_numericcheck_with_units(self):
        from moho.core._attributes import _NumericAttribute as NumericAttribute

        a = NumericAttribute()
        a.link(wid, 'a')
        a.link_deps(wid, 'a')
        self.assertEqual(a.get(wid), 0)
        a.set(wid, '99px')
        self.assertEqual(a.get(wid), 99)
        self.assertEqual(a.get_units(wid), 'px')
        
        b = NumericAttribute(units="%")
        b.link(wid, 'b')
        b.link_deps(wid, 'b')
        self.assertEqual(b.get(wid), 0)
        b.set(wid, 99)
        self.assertEqual(b.get(wid), 99)
        self.assertEqual(b.get_units(wid), '%')
        

    def test_listcheck(self):
        from moho.core._attributes import _ListAttribute as ListAttribute

        a = ListAttribute()
        a.link(wid, 'a')
        a.link_deps(wid, 'a')
        self.assertEqual(a.get(wid), [])
        a.set(wid, [1, 2, 3])
        self.assertEqual(a.get(wid), [1, 2, 3])

    def test_bounded_numeric_property(self):
        from moho.core._attributes import _BoundedNumericAttribute as BoundedNumericAttribute

        bnp = BoundedNumericAttribute(0.0, min=0.0, max=3.5)

        bnp.link(wid, 'bnp')

        bnp.set(wid, 1)
        bnp.set(wid, 0.0)
        bnp.set(wid, 3.1)
        bnp.set(wid, 3.5)
        self.assertRaises(ValueError, partial(bnp.set, wid, 3.6))
        self.assertRaises(ValueError, partial(bnp.set, wid, -3))

    def test_bounded_numeric_property_error_value(self):
        from moho.core._attributes import _BoundedNumericAttribute as BoundedNumericAttribute

        bnp = BoundedNumericAttribute(0, min=-5, max=5, errorvalue=1)
        bnp.link(wid, 'bnp')

        bnp.set(wid, 1)
        self.assertEqual(bnp.get(wid), 1)

        bnp.set(wid, 5)
        self.assertEqual(bnp.get(wid), 5)

        bnp.set(wid, 6)
        self.assertEqual(bnp.get(wid), 1)

        bnp.set(wid, -5)
        self.assertEqual(bnp.get(wid), -5)

        bnp.set(wid, -6)
        self.assertEqual(bnp.get(wid), 1)

    def test_html_attribute_attrname(self):
        from moho.core._attributes import _Attribute as Attribute

        attr = Attribute('value', attrname="custom_name")

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.attrname, 'custom_name')

    def test_html_attribute_without_options(self):
        from moho.core._attributes import _Attribute as Attribute

        attr = Attribute('attribute_value')

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.get(wid), 'attribute_value')
        self.assertEqual(attr.attrvalue(wid), 'attribute_value')
        self.assertEqual(attr.attrname, 'attr')

        attr.set(wid, "new_attribute_value")
        self.assertEqual(attr.get(wid), 'new_attribute_value')
        self.assertEqual(attr.attrvalue(wid), 'new_attribute_value')

    def test_html_attribute_with_options(self):
        from moho.core._attributes import _Attribute as Attribute

        attr = Attribute('1', options=["1", "2", "3"])

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.get(wid), '1')
        self.assertEqual(attr.attrvalue(wid), '1')
        self.assertEqual(attr.attrname, 'attr')

        with self.assertRaises(ValueError) as cm:
            attr.set(wid, "bad_value")

        attr.set(wid, "3")
        self.assertEqual(attr.get(wid), '3')
        self.assertEqual(attr.attrvalue(wid), '3')

    def test_html_attribute_with_options_values(self):
        from moho.core._attributes import _Attribute as Attribute

        attr = Attribute('1', options=["1", "2", "3"],
                         values=["value-1", "value-2", "value-3"])

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.get(wid), '1')
        self.assertEqual(attr.attrvalue(wid), 'value-1')
        self.assertEqual(attr.attrname, 'attr')

        with self.assertRaises(ValueError) as cm:
            attr.set(wid, "bad_value")

        attr.set(wid, "3")
        self.assertEqual(attr.get(wid), '3')
        self.assertEqual(attr.attrvalue(wid), 'value-3')

    def test_css_attribute_with_options(self):
        from moho.core._attributes import _Attribute as Attribute

        attr = Attribute(
            '1', options=["1", "2", "3"], attrname="custom_name")

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.get(wid), '1')
        self.assertEqual(attr.attrname, 'custom_name')
        self.assertEqual(attr.attrvalue(wid), '1')

        with self.assertRaises(ValueError) as cm:
            attr.set(wid, "bad_value")

        attr.set(wid, "3")
        self.assertEqual(attr.get(wid), '3')
        self.assertEqual(attr.attrvalue(wid), '3')

    def test_css_numeric_attribute_with_options(self):
        from moho.core._attributes import _NumericAttribute as NumericAttribute

        attr = NumericAttribute(
            10, options=["custom-1", "custom-2", "custom-3"],
            attrname="custom_name", units='')

        attr.link(wid, 'attr')
        attr.link_deps(wid, 'attr')
        self.assertEqual(attr.get(wid), 10)
        self.assertEqual(attr.attrname, 'custom_name')
        self.assertEqual(attr.attrvalue(wid), '10')

        attr.set(wid, 30)
        self.assertEqual(attr.get(wid), 30)
        self.assertEqual(attr.attrvalue(wid), '30')

        attr.set(wid, '30in')
        self.assertEqual(attr.get(wid), 30)
        self.assertEqual(attr.attrvalue(wid), '30in')

        with self.assertRaises(ValueError):
            attr.set(wid, "bad_value")

        with self.assertRaises(ValueError):
            attr.set(wid, "30i")

        attr.set(wid, "custom-1")
        self.assertEqual(attr.get(wid), "custom-1")
        self.assertEqual(attr.attrvalue(wid), 'custom-1')

    def test_css_color_attribute(self):
        from moho.core._attributes import _ColorAttribute as ColorAttribute

        color = ColorAttribute(format="css")
        color.link(wid, 'color')
        color.link_deps(wid, 'color')
        color.set(wid, "#ffffff")
        self.assertEqual(color.get(wid), [255, 255, 255])

        color.set(wid, "#00ff00")
        self.assertEqual(color.get(wid), [0, 255, 0])
        self.assertEqual(color.attrvalue(wid), 'rgb(0, 255, 0)')

        color.set(wid, "#7f7fff7f")
        self.assertEqual(color.get(wid)[0], 127)
        self.assertEqual(color.get(wid)[1], 127)
        self.assertEqual(color.get(wid)[2], 255)
        self.assertEqual(color.get(wid)[3], 127 / 255.)

        color.set(wid, (255, 255, 0))
        self.assertEqual(color.get(wid), [255, 255, 0])
        color.set(wid, (255, 255, 0, 0))
        self.assertEqual(color.get(wid), [255, 255, 0, 0])
