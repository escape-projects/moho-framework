'''
Element class
=============
 
In Moho any visual component of application, i.e. widget, is inherited from the class :class:`Element`.
This class defines standard  for all html elements attributes and initializes
all necessary connections between backend and frontend events. 

The Element class follows several principles:

* *Event Driven*

  Elements can interact with each other on backend side via events. If a property
  changes the element can respond to the change in the 'on_<propname>'
  callback. That's the main goal of the :class:`~moho.core.properties.Property` class.
  Elements can also interact with their frontend representation via attributes. 
  Each attribute is an advanced Property, thus, when element changes attribute value
  'on_<attrname>' is called, plus a remote event between element instance on server side and
  html tag on client side is triggered.
  
* *Separation Of Concerns (the element and its graphical representation)*

  We tried to minimize the amount of web programming involved to the process
  of creating an application. However minimal knowledge of Jquery library and HTML
  is still required for customisation of elements.
  Each element has a graphical representation which is an html code template.
  'html_tpl' string property holds this template. 'js_tpl' is another template property
  for javascript code.  

Using Properties and Attributes
-------------------------------

:class:`Property` is used for declaration of properties of elements. Properties are relevant
only for backend side. A property which handles value of html element attribute must be declared
:class:`Attribute`. There are several types of attributes::

    * Style attributes, to change element's css style attributes
    * Class attributes, to change css classes html element belongs to
    * Element attributes, other attributes of html element, which are not style or class attributes

All properties and attributes are described in the format:: 
    <name> is a <property class> and defaults to <default value>.
    e.g. 
    :attr:`~moho.uix.element.Element.children` is a
    :class:`~moho.core.properties.ListProperty` and defaults to [].

If you want to be notified when new children are added to the element, 
you can bind your own callback function like this::
    def callback_children(instance, value):
        print('Children:', value)
    el = Element()
    el.bind(children=callback_children)

Read more about :doc:`api-moho.core.properties` and :doc:`api-moho.core.attributes`.

'''

from moho.core.compat import iteritems
from moho.core.handlers import MainWebSocketHandler

__all__ = ('ElementException', 'Element', 'HtmlElement')

from moho.core.event import EventDispatcher
from moho.core.factory import Factory
from moho.core.properties import (StringProperty, AliasProperty, DictProperty,
                                  ListProperty, ObjectProperty, BooleanProperty,
                                  OptionProperty)

from moho.core._attributes import _BooleanAttribute
from moho.core.attributes import (
    ElementAttribute,
    StyleAttribute, StyleNumericAttribute,
    StyleVariableListAttribute,
    StyleColorAttribute, EventAttribute, ElementBooleanAttribute, ElementNumericAttribute,
    ElementLogicalAttribute)

from moho.lang import Builder
from moho.core.weakproxy import WeakProxy

from moho.core.app import WebApp
from moho.core.registry import element_registry

from functools import partial
from itertools import islice

from tornado.template import Template
import tornado

# References to all the widget destructors (partial method with element uid as
# key).
_element_destructors = {}


def _element_destructor(uid, r):
    # Internal method called when a element is deleted from memory. the only
    # thing we remember about it is its uid. Clear all the associated callbacks
    # created in kv language.
    del _element_destructors[uid]
    Builder.unbind_element(uid)


class ElementException(Exception):
    '''Fired when the element gets an exception.
    '''
    pass


class ElementMetaclass(type):
    '''Metaclass to automatically register new elements for the
    :class:`~moho.core.factory.Factory`.

    .. warning::
        This metaclass is used by the Element. Do not use it directly!
    '''

    def __init__(mcs, name, bases, attrs):
        super(ElementMetaclass, mcs).__init__(name, bases, attrs)
        ign = mcs.__factory_ignore__
        if not ign:
            Factory.register(name, cls=mcs)


# : Base class used for HtmlElement, that inherits from :class:`EventDispatcher`
ElementBase = ElementMetaclass('ElementBase', (EventDispatcher,),
                               {"__factory_ignore__": False})


class Element(ElementBase):
    '''Element base class. This is base class for all elements. It contains methods
    for connection of attributes and properties to application instance.
    This class doesn't have declaration of any default html attribute.
    '''
    
    __metaclass__ = ElementMetaclass

    _proxy_ref = None

    __accepted_children_types__ = []

    def __init__(self, **kwargs):
        element_registry[id(self)] = self

        self._content = ''
        no_builder = '__no_builder' in kwargs
        if no_builder:
            del kwargs['__no_builder']
        on_args = {k: v for k, v in kwargs.items() if k[:3] == 'on_'}
        for key in on_args:
            del kwargs[key]

        super(Element, self).__init__(**kwargs)

        # Apply all the styles.
        if not no_builder:
            Builder.apply(self, ignored_consts=self._kwargs_applied_init)

        # Bind all the events.
        if on_args:
            self.bind(**on_args)
            
        self._initialize_attributes()

    def _initialize_attributes(self):
        for _, pcls in iteritems(self.properties()):
            attrtype = getattr(pcls, 'attrtype', None)
            # print pname
            if attrtype == 'style':
                name, value = pcls.attrname, pcls.attrvalue(self)
                self.style_container[name] = value
                pcls.fbind(self, self.update_style, 0, (pcls,))
            elif attrtype == 'class':
                name, value = pcls.attrname, pcls.attrvalue(self)
                self.cls_container[name] = value
                pcls.fbind(self, self.update_class, 0, (pcls,))
            elif attrtype == 'element':
                name, value = pcls.attrname, pcls.attrvalue(self)
                self.attr_container[name] = value
                if getattr(pcls, 'remote', False):
                    pcls.fbind(self, self.update_attribute, 0, (pcls,))

    @property
    def proxy_ref(self):
        '''Return a proxy reference to the element, i.e. without creating a
        reference to the element. See `weakref.proxy
        <http://docs.python.org/2/library/weakref.html?highlight\
        =proxy#weakref.proxy>`_ for more information.
        '''
        _proxy_ref = self._proxy_ref
        if _proxy_ref is not None:
            return _proxy_ref

        f = partial(_element_destructor, self.uid)
        self._proxy_ref = _proxy_ref = WeakProxy(self, f)
        # Only f should be enough here, but it appears that is a very
        # specific case, the proxy destructor is not called if both f and
        # _proxy_ref are not together in a tuple.
        _element_destructors[self.uid] = (f, _proxy_ref)
        return _proxy_ref

    @property
    def application(self):
        '''Return reference to the current application'''
        return WebApp.get_running_app()

    def __hash__(self):
        return id(self)

    @property
    def __self__(self):
        return self

    #
    # Tree management
    #
    def add_element(self, element, index=0):
        '''Add a new element as a child of this element.

        :Parameters:
            `element`: :class:`Element`
                Element to add to our list of children.
            `index`: int, defaults to 0
                Index to insert the element in the list. Notice that the default
                of 0 means the element is inserted at the beginning of the list
                and will thus be drawn on top of other sibling elements. 
        '''
        if not isinstance(element, Element):
            raise ElementException(
                'add_element() can be used only with instances'
                ' of the Element class.')

        acc = self.__accepted_children_types__
        if acc and not isinstance(element, tuple(acc)):
            raise ElementException(
                'unexpected element type {0}'.format(
                    type(element)))

        element = element.__self__
        if element is self:
            raise ElementException(
                'Element instances cannot be added to themselves.')
        parent = element.parent
        # Check if the element is already a child of another element.
        if parent:
            raise ElementException('Cannot add %r, it already has a parent %r'
                                   % (element, parent))
        element.parent = parent = self

        if index == 0 or len(self.children) == 0:
            self.children.insert(0, element)
        else:
            children = self.children
            if index >= len(children):
                index = len(children)
            children.insert(index, element)
            
    def add_elements(self, elements, index=0):
        '''Add new elements as children of this element.

        :Parameters:
            `elements`: :list of class:`Element` instances
                Elements to add to our list of children.
            `index`: int, defaults to 0
                Index to insert the elements in the list. Notice that the default
                of 0 means the element is inserted at the beginning of the list
                and will thus be drawn on top of other sibling elements. 
        '''
        if not isinstance(elements, list):
            raise ElementException(
                'add_elements() can be used only with list of instances'
                ' of the Element class.')
        if not all([isinstance(el, Element) for el in elements]):
            raise ElementException(
                'add_element() can be used only with instances'
                ' of the Element class.')
        children = self.children[:]
        for el in elements:
            acc = self.__accepted_children_types__
            if acc and not isinstance(el, tuple(acc)):
                raise ElementException(
                    'unexpected element type {0}'.format(
                        type(el)))
    
            element = el.__self__
            if element is self:
                raise ElementException(
                    'Element instances cannot be added to themselves.')
            parent = element.parent
            # Check if the element is already a child of another element.
            if parent:
                raise ElementException('Cannot add %r, it already has a parent %r'
                                       % (element, parent))
            element.parent = parent = self
            # Child will be disabled if added to a disabled parent.
    
            if index == 0 or len(children) == 0:
                children.insert(0, el)
            else:
                if index >= len(children):
                    index = len(children)
                children.insert(index, el)
        self.children = children

    def remove_element(self, element):
        '''Remove an element from the children of this element.

        :Parameters:
            `element`: :class:`Element`
                Element to remove from our children list.

            '''
        if element not in self.children:
            return
        self.children.remove(element)

    def clear_elements(self, children=None):
        '''
        Remove all (or the specified) :attr:`~Element.children` of this element.
        If the 'children' argument is specified, it should be a list (or
        filtered list) of children of the current element.
        '''

        if not children:
            children = self.children
        remove_element = self.remove_element
        for child in children[:]:
            remove_element(child)

    def _walk(self, restrict=False, loopback=False, index=None):
        # We pass index only when we are going on the parent
        # so don't yield the parent as well.
        if index is None:
            index = len(self.children)
            yield self

        for child in reversed(self.children[:index]):
            for walk_child in child._walk(restrict=True):
                yield walk_child

        # If we want to continue with our parent, just do it.
        if not restrict:
            parent = self.parent
            try:
                if parent is None or not isinstance(parent, Element):
                    raise ValueError
                index = parent.children.index(self)
            except ValueError:
                # Self is root, if we want to loopback from the first element:
                if not loopback:
                    return
                # If we started with root (i.e. index==None), then we have to
                # start from root again, so we return self again. Otherwise, we
                # never returned it, so return it now starting with it.
                parent = self
                index = None
            for walk_child in parent._walk(loopback=loopback, index=index):
                yield walk_child

    def walk(self, restrict=False, loopback=False):
        ''' Iterator that walks the element tree starting with this element and
        goes forward returning elements.

        :Parameters:
            `restrict`: bool, defaults to False
                If True, it will only iterate through the element and its
                children (or children of its children etc.). Defaults to False.
            `loopback`: bool, defaults to False
                If True, when the last element in the tree is reached,
                it'll loop back to the uppermost root and start walking until
                we hit this element again. Naturally, it can only loop back when
                `restrict` is False. Defaults to False.

        :return:
            A generator that walks the tree, returning elements in the
            forward layout order.

        '''
        gen = self._walk(restrict, loopback)
        yield next(gen)
        for node in gen:
            if node is self:
                return
            yield node

    def _walk_reverse(self, loopback=False, go_up=False):
        # process is walk up level, walk down its children tree, then walk up
        # next level etc.
        # default just walk down the children tree
        root = self
        index = 0
        # we need to go up a level before walking tree
        if go_up:
            root = self.parent
            try:
                if root is None or not isinstance(root, Element):
                    raise ValueError
                index = root.children.index(self) + 1
            except ValueError:
                if not loopback:
                    return
                index = 0
                go_up = False
                root = self

        # now walk children tree starting with last-most child
        for child in islice(root.children, index, None):
            for walk_child in child._walk_reverse(loopback=loopback):
                yield walk_child
        # we need to return ourself last, in all cases
        yield root

        # if going up, continue walking up the parent tree
        if go_up:
            for walk_child in root._walk_reverse(loopback=loopback,
                                                 go_up=go_up):
                yield walk_child

    def walk_reverse(self, loopback=False):
        ''' Iterator that walks the element tree backwards starting with the
        element before this, and going backwards returning elements in the
        reverse order.

        This walks in the opposite direction of :meth:`walk`, so a list of the
        tree generated with :meth:`walk` will be in reverse order compared
        to the list generated with this, provided `loopback` is True.

        :Parameters:
            `loopback`: bool, defaults to False
                If True, when the uppermost root in the tree is
                reached, it'll loop back to the last element and start walking
                back until after we hit element again. Defaults to False.

        :return:
            A generator that walks the tree, returning elements in the
            reverse layout order.

        '''
        for node in self._walk_reverse(loopback=loopback, go_up=True):
            yield node
            if node is self:
                return

    id = StringProperty(None, allownone=True)
    '''Unique identifier of the element in the tree.

    :attr:`id` is a :class:`~moho.properties.StringProperty` and defaults to
    None.

    .. warning::

        If the :attr:`id` is already used in the tree, an exception will
        be raised.
    '''

    ids = DictProperty({})

    children = ListProperty([])
    '''List of children of this element.

    :attr:`children` is a :class:`~moho.core.properties.ListProperty` and
    defaults to an empty list.

    Use :meth:`add_widget` and :meth:`remove_widget` for manipulating the
    children list. Don't manipulate the children list directly unless you know
    what you are doing.
    '''

    parent = ObjectProperty(None, allownone=True, rebind=True)
    '''Parent of this element. The parent of an element is set when the element
    is added to another element and unset when the element is removed from its
    parent.

    :attr:`parent` is an :class:`~moho.core.properties.ObjectProperty` and
    defaults to None.
    '''

    websocket_cls = ObjectProperty(None)
    '''Custom web socket class for this element.
    
    :attr:`websocket_cls` is an :class:`~moho.core.properties.ObjectProperty` and
    defaults to None.
     
    '''
    
    websocket_host = StringProperty("")
    '''Custom web socket host pattern for this element.
    
    :attr:`websocket_host` is an :class:`~moho.core.properties.StringProperty` and
    defaults to "".
     
    '''
    
    stylesheet = StringProperty('')
    '''Additional css stylesheet file which is required by the element. Path 
    to the stylesheet file will be added to the head of index.html file.'''

    update_content_on_children = BooleanProperty(True)
    '''If True the content of an element will be updated when children property is changed.
    This is internal property, please use it when you know what you are doing.
    
    '''

    def get_tagid(self):
        return 'el{0}'.format(id(self))

    tagid = AliasProperty(get_tagid, None)
    '''Return unique element id. It is used for html id attribute.'''

    css_declarations = DictProperty()
    '''Container for all css declarations for this element. This property is for internal usage only.
    '''

    style_container = DictProperty()
    '''Container for all style attributes. This property is for internal usage only.'''

    default_cls = ListProperty()
    '''List of default element classes. CSS classes from this list will be added to class attribute by default'''

    cls_container = DictProperty()
    '''Container for class attributes. This property is for internal usage only.'''

    attr_container = DictProperty()
    '''Container for element attributes.'''

    def get_style(self):
        items = ["{0}: {1}".format(key, value)
                 for (key, value) in iteritems(self.style_container) if value]
        return ";".join(items)

    style = AliasProperty(get_style, None, bind=('style_container',))
    '''Return generated string for style attributes'''

    def get_css(self):
        if not self.css_declarations:
            return ''
        return ("#{id} {{ {decls};}}\n".format(
            id=self.tagid,
            decls=";".join([
                "{0}: {1}".format(key, value)
                for (key, value) in iteritems(self.css_declarations)])))

    css = AliasProperty(get_css, None, bind=('css_declarations',))
    '''Return generated string for css instructions.'''
    
    def get_cls(self):
        items = self.default_cls[:]
        items.extend([value for value in self.cls_container.values() if value])
        return " ".join(items)

    cls = AliasProperty(get_cls, None, bind=('default_cls', 'cls_container'))
    '''Return generated string for "class" property.'''
    
    def get_attributes(self):
        items = []
        for (key, value) in iteritems(self.attr_container):
            if not value:
                continue
            if key == value:
                items.append(value)
            else:
                items.append('{0}="{1}"'.format(key, value))
        return " ".join(items)

    attributes = AliasProperty(get_attributes, None, bind=('attr_container',))
    '''Return generated string containing all element attributes.'''
    
    tagname = "unnamed"

    html_tpl = StringProperty(
        """<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}"""
        """{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %} >{%raw obj.content%}</{{obj.tagname}}>"""
    )

    js_tpl = StringProperty('')
    '''This property contains javascript for this element. 
    Javascript code generated from this element will be added '''
    
    def get_content(self):
        cnt = ""
        if self.children:
            cnt += "\n".join([ch.html for ch in reversed(self.children)])
        cnt += self._content
        return cnt

    def set_content(self, cnt):
        self._content = str(cnt)
        return True

    # we do not bind to children
    content = AliasProperty(get_content, set_content)
    '''Return string containing html code for children of this element.'''
    
    def get_html(self):
        html = tornado.escape.native_str(
            Template(self.html_tpl).generate(obj=self))
        js = tornado.escape.native_str(
            Template(self.js_tpl).generate(obj=self))
        if js:
            return "\n".join([html, "<script>", js, "</script>"])
        return html

    html = AliasProperty(get_html, None)
    '''Return string containing full html code of this element.'''

    def bind_to_app(self):
        '''Bind attributes and properties to application slots responsible for 
        frontend updates.'''
        app = self.application
        content_prop = self.property("content")
        self.bind(style=app.update_element_css,
                  cls=app.update_element_class,
                  content=app.update_element_content)
        if self.update_content_on_children:
            self.bind(style=app.update_element_css,
                      children=content_prop.trigger_change)

    def unbind_from_app(self):
        '''Unbind attributes and properties of this element from application.'''
        app = self.application
        content_prop = self.property("content")
        self.unbind(style=app.update_element_css,
                    cls=app.update_element_class,
                    children=content_prop.trigger_change,
                    content=app.update_element_content)

    def update_style(self, prop, obj, value):
        self.style_container[prop.attrname] = prop.attrvalue(obj)

    def update_class(self, prop, obj, value):
        self.cls_container[prop.attrname] = prop.attrvalue(obj)

    def update_attribute(self, prop, obj, value):
        app = self.application
        pname = prop.attrname
        pval = prop.attrvalue(obj)
        self.attr_container[pname] = pval
        if getattr(prop, "remote", False):
            if isinstance(prop, _BooleanAttribute):
                app.update_boolean_attr(self, pname, prop.get(obj))
            else:
                app.update_element_attr(self, pname, pval)

    def remote_dispatch(self, event, **kwargs):
        '''Perform dispatching of event from this backend element to the
        corresponding frontend element.
        '''
        self.application.remote_dispatch(
            MainWebSocketHandler.alias, self, event, **kwargs)


class HtmlElement(Element):
    '''
    HtmlElement class declares default attributes and events for a html element.
    These attributes have the same name as in html documentation. The undescore 
    in style attributes names is automatically replaced by '-'. 
    '''
    
    size_hint_xs = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_sm = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_md = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_lg = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_xl = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_xxl = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)
    
    script = StringProperty('')
    '''Path to javascript file which contains functionality relevant to this element.
    This path will appear in the head of generated html code.'''
    
    accesskey = ElementAttribute(None)

    draggable = ElementAttribute(
        None,
        options=["true", "false", "auto"]
    )

    dropzone = ElementAttribute(None,
                                options=["", "copy", "move", "link"])

    contenteditable = ElementLogicalAttribute(None, remote=True)

    hidden = ElementBooleanAttribute(False, remote=True)

    tabindex = ElementNumericAttribute(None, remote=True, units="")

    title = ElementAttribute(None, remote=True)

    width = StyleNumericAttribute(
        None, options=["auto", "initial", "inherit"])
    
    flex_grow = StyleNumericAttribute(
        None, options=["inherit", "initial"])
    
    flex_shrink = StyleNumericAttribute(
        None, options=["inherit", "initial"])
    
    flex_basis = StyleNumericAttribute(
        None, options=["inherit", "auto", "initial"])
    
    overflow_x = StyleAttribute(
        None, options=["visible", "hidden",
                       "scroll", "auto",
                       "initial", "inherit"])
        
    overflow_y = StyleAttribute(
        None, options=["visible", "hidden",
                       "scroll", "auto",
                       "initial", "inherit"])
    
    height = StyleNumericAttribute(
        None, options=["auto", "initial", "inherit"])

    min_width = StyleNumericAttribute(
        None, options=["none", "initial", "inherit"])

    min_height = StyleNumericAttribute(
        None, options=["none", "initial", "inherit"])

    max_width = StyleNumericAttribute(
        None, options=["none", "initial", "inherit"])

    max_height = StyleNumericAttribute(
        None, options=["none", "initial", "inherit"])

    padding = StyleVariableListAttribute(
        None, options=["initial", "inherit"])

    margin = StyleVariableListAttribute(
        None, options=["initial", "inherit"])

    background_color = StyleColorAttribute(
        None,
        options=["transparent", "initial", "inherit"])

    background_image = StyleAttribute(
        None, options=["none", "initial", "inherit"])

    background_repeat = StyleAttribute(
        None,
        options=["repeat", "repeat-x", "repeat-y", "no-repeat",
                 "initial", "inherit"
                 ]
    )

    background_position = StyleVariableListAttribute(
        None, max_length=2,
        options=['',
                 "initial", "inherit", "left",
                 "top", "bottom", "center"]
    )

    # background_size = StyleVariableListAttribute(
    #    'auto', options=["auto", "cover", "contain", "initial", "inherit"]
    # )

    color = StyleColorAttribute(None,
                                options=["initial", "inherit"])

    font_size = StyleNumericAttribute(
        None,
        options=["medium", "xx-small", "x-small",
                 "small", "large", "x-large",
                 "xx-large", "initial", "inherit"]
    )

    line_height = StyleNumericAttribute(
        None, options=["normal", "initial", "inherit"])

    letter_spacing = StyleNumericAttribute(
        None, options=["normal", "initial", "inherit"])

    font_family = StyleAttribute("")

    font_style = StyleAttribute(
        None,
        options=["normal", "italic", "oblique", "initial", "inherit"])

    font_weight = StyleAttribute(
        None,
        options=["normal", "bold", "bolder",
                 "lighter", "initial", "inherit"]
    )

    onkeydown = EventAttribute(
        False,
        eventargs={
            "key": "event.key",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.altKey"}
    )

    onkeyup = EventAttribute(
        False,
        eventargs={
            "key": "event.key",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.altKey"}
    )

    onkeypress = EventAttribute(
        False,
        eventargs={
            "key": "event.key",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey"}
    )

    onclick = EventAttribute(
        False,
        eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    ondblclick = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onmousedown = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onmouseup = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onmousemove = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onmouseout = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onmouseover = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onwheel = EventAttribute(
        False, eventargs={
            "button": "event.button",
            "buttons": "event.buttons",
            "altKey": "event.altKey",
            "ctrlKey": "event.ctrlKey",
            "shiftKey": "event.shiftKey",
            "clientX": "event.clientX",
            "clientY": "event.clientY",
            "detail": "event.detail",
            "pageX": "event.pageX",
            "pageY": "event.pageY",
        }
    )

    onblur = EventAttribute(False)

    oncontextmenu = EventAttribute(False)

    onfocus = EventAttribute(False)

    def on_remote_keypress(self, **kwargs):
        pass

    def on_remote_keydown(self, **kwargs):
        pass

    def on_remote_keyup(self, **kwargs):
        pass

    def on_remote_click(self, **kwargs):
        pass

    def on_remote_dblclick(self, **kwargs):
        pass

    def on_remote_mousedown(self, **kwargs):
        pass

    def on_remote_mousemove(self, **kwargs):
        pass

    def on_remote_mouseout(self, **kwargs):
        pass

    def on_remote_mouseover(self, **kwargs):
        pass

    def on_remote_mouseup(self, **kwargs):
        pass

    def on_remote_wheel(self, **kwargs):
        pass

    def on_remote_blur(self, **kwargs):
        pass

    def on_remote_focus(self, **kwargs):
        pass

    def on_remote_contextmenu(self, **kwargs):
        pass


class NoContentHtmlElement(HtmlElement):
    '''Represent html element without content, like <img>, <input>, etc'''
    
    def add_element(self, element, index=0):
        raise Exception("adding elements is not supported")
