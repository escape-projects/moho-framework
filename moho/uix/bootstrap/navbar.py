"""
Navbar
======================

"""

from moho.core.properties import StringProperty, ObjectProperty
from moho.uix.html5.hyperlink import HyperLink
from moho.uix.bootstrap.button import Button
from moho.uix.bootstrap.dropdown import DropdownItem, DropdownDivider, DropdownHeader
from moho.core.attributes import ClassAttribute
from moho.uix.bootstrap.element import BootstrapElement


class NavbarBrand(HyperLink):

    text = StringProperty('')


class NavbarLink(HyperLink):

    text = StringProperty('')

    html_tpl = StringProperty(
        '<li class="nav-item">'
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %}'
        ' class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %}'
        ' {% raw obj.attributes %}>'
        '{%raw obj.text%}'
        '{%raw obj.content%}'
        '</{{obj.tagname}}>'
        '</li>'
    )


class NavbarDropdown(HyperLink):

    text = StringProperty('')

    html_tpl = StringProperty(
        '<li class="nav-item dropdown">'
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %}'
        ' class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %}'
        ' {% raw obj.attributes %} role="button"'
        ' data-bs-toggle="dropdown" aria-expanded="false">'
        '{%raw obj.text%}'
        '</{{obj.tagname}}>'
        '<ul class="dropdown-menu" aria-labelledby="{{obj.tagid}}">'
        '{%raw obj.content%}'
        '</ul>'
        '</li>'
    )

    def on_remote_click(self, *args):
        pass

    __accepted_children_types__ = [
        DropdownItem,
        DropdownDivider,
        DropdownHeader
    ]


class Navbar(BootstrapElement):

    tagname = "nav"

    brand = ObjectProperty(None)

    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %}'
        ' class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %}'
        ' {% raw obj.attributes %}>'
        '<div class="container-fluid">'
        '{% if obj.brand is not None %} {%raw obj.brand.html%} {% end %}'
        '<button class="navbar-toggler" type="button"'
        ' data-bs-toggle="collapse"'
        ' data-bs-target="#{{obj.tagid}}_dropdown"'
        ' aria-controls="{{obj.tagid}}_dropdown" aria-expanded="false"'
        ' aria-label="Toggle navigation">'
        '<span class="navbar-toggler-icon"></span></button>'
        '<div class="collapse navbar-collapse" id="{{obj.tagid}}_dropdown">'
        '<ul class="navbar-nav">'
        '{%raw obj.content%}'
        '</ul>'
        '</div>'
        '</div>'
        '</{{obj.tagname}}>'
    )

    background_color = ClassAttribute(
        'primary',
        options=["primary", "secondary", "success",
                 "danger", "warning", "info",
                 "light", "dark", "white"
                 ],
        values=["bg-primary", "bg-secondary",
                "bg-success", "bg-danger",
                "bg-warning", "bg-info",
                "bg-light",
                "bg-dark", "bg-white"]
    )

    theme = ClassAttribute(
        'light',
        options=["light", "dark"
                 ],
        values=["navbar-light", "navbar-dark"]
    )

    placement = ClassAttribute(
        None,
        options=["top", "bottom", 'sticky_top'
                 ],
        values=["fixed-top", "fixed-bottom", 'sticky-top']
    )

    def add_element(self, element, index=0):
        if isinstance(element, NavbarBrand):
            self.brand = element
        else:
            BootstrapElement.add_element(self, element, index=index)

    __accepted_children_types__ = [NavbarBrand, NavbarLink,
                                   NavbarDropdown, Button]
