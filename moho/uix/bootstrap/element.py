'''
Bootstrap Element class
=========================

'''
from moho.core.properties import OptionProperty
from moho.uix.element import HtmlElement


class BootstrapElement(HtmlElement):

    size_hint_xs = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_sm = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_md = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_lg = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_xl = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)

    size_hint_xxl = OptionProperty(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"], allownone=True)
