"""
Application
======================

"""

import os

from moho import moho_data_dir
from moho.core.app import WebApp
from moho.core.factory import Factory
from moho.core.properties import (
    ListProperty)
from moho.lang.builder import Builder
from moho.uix.body import Body
# from moho.uix.form import LoginForm

r = Factory.register

# registering classes in the factory

r('HtmlElement', module='moho.uix.element')
r('BootstrapElement', module='moho.uix.bootstrap.element')

r('Heading', module='moho.uix.html5.heading')
r('H1', module='moho.uix.html5.heading')
r('H2', module='moho.uix.html5.heading')
r('H3', module='moho.uix.html5.heading')
r('H4', module='moho.uix.html5.heading')
r('H5', module='moho.uix.html5.heading')

r('Paragraph', module='moho.uix.html5.paragraph')
r('SmallText', module='moho.uix.html5.paragraph')
r('Span', module='moho.uix.html5.paragraph')
r('Image', module='moho.uix.html5.image')
r('Div', module='moho.uix.html5.div')

r('BoxLayout', module='moho.uix.bootstrap.boxlayout')
r('HBoxLayout', module='moho.uix.bootstrap.boxlayout')
r('VBoxLayout', module='moho.uix.bootstrap.boxlayout')

r('GridLayout', module='moho.uix.bootstrap.gridlayout')
r('Row', module='moho.uix.bootstrap.gridlayout')
r('Col', module='moho.uix.bootstrap.gridlayout')

r('Button', module='moho.uix.bootstrap.button')
r('Toolbar', module='moho.uix.bootstrap.toolbar')

r('Input', module='moho.uix.bootstrap.input')
r('InputHint', module='moho.uix.bootstrap.input')
r('InputLabel', module='moho.uix.bootstrap.input')

r('Checkbox', module='moho.uix.bootstrap.checkbox')
r('Range', module='moho.uix.bootstrap.range')

r('Radio', module='moho.uix.bootstrap.radio')
r('RadioGroup', module='moho.uix.bootstrap.radio')

r('LineEdit', module='moho.uix.bootstrap.lineedit')
r('FloatingLineEdit', module='moho.uix.bootstrap.lineedit')

r('TextArea', module='moho.uix.bootstrap.textedit')
r('TextEdit', module='moho.uix.bootstrap.textedit')
r('FloatingTextEdit', module='moho.uix.bootstrap.textedit')

r('Select', module='moho.uix.bootstrap.combobox')
r('Combobox', module='moho.uix.bootstrap.combobox')
r('FloatingCombobox', module='moho.uix.bootstrap.combobox')

r('Alert', module='moho.uix.bootstrap.alert')

r('ProgressBar', module='moho.uix.bootstrap.progress')
r('Progress', module='moho.uix.bootstrap.progress')

r('MplFigure', module='moho.uix.bootstrap.mpl.figure')
r('Map3d', module='moho.uix.bootstrap.mpl.map3d')
r('Plot2d', module='moho.uix.bootstrap.mpl.plot2d')
r('ErrorbarPlot', module='moho.uix.bootstrap.mpl.errorbar')
r('BarPlot', module='moho.uix.bootstrap.mpl.bar')
r('Scatter', module='moho.uix.bootstrap.mpl.scatter')

r('TabPanel', module='moho.uix.bootstrap.tabs')
r('Tab', module='moho.uix.bootstrap.tabs')

r('CardBody', module='moho.uix.bootstrap.card')
r('CardImage', module='moho.uix.bootstrap.card')
r('Card', module='moho.uix.bootstrap.card')

r('CardDeck', module='moho.uix.bootstrap.card')
r('Accordion', module='moho.uix.bootstrap.accordion')
r('AccordionItem', module='moho.uix.bootstrap.accordion')

r('Modal', module='moho.uix.bootstrap.modal')
r('ModalHeader', module='moho.uix.bootstrap.modal')
r('ModalBody', module='moho.uix.bootstrap.modal')
r('ModalFooter', module='moho.uix.bootstrap.modal')

r('Navbar', module='moho.uix.bootstrap.navbar')
r('NavbarBrand', module='moho.uix.bootstrap.navbar')
r('NavbarLink', module='moho.uix.bootstrap.navbar')
r('NavbarDropdown', module='moho.uix.bootstrap.navbar')

r('Sidebar', module='moho.uix.bootstrap.sidebar')
r('SidebarHeader', module='moho.uix.bootstrap.sidebar')
r('SidebarBody', module='moho.uix.bootstrap.sidebar')

r('Toast', module='moho.uix.bootstrap.toast')
r('ToastHeader', module='moho.uix.bootstrap.toast')
r('ToastBody', module='moho.uix.bootstrap.toast')

r('Dropdown', module='moho.uix.bootstrap.dropdown')

r('Spinner', module='moho.uix.bootstrap.spinner')
r('GrowSpinner', module='moho.uix.bootstrap.spinner')

r('Svg', module='moho.uix.svg')

r('X3d', module='moho.uix.x3d')
r('X3dScene', module='moho.uix.x3d.scene')
r('X3dShape', module='moho.uix.x3d.shape')

Builder.load_file(
    os.path.join(moho_data_dir, 'bootstrap.mh'), rulesonly=True)

# class BSLoginForm(LoginForm):

#    js_tpl = StringProperty("""
#        $('#{{obj.tagid}}').on("on_login", null, function(e, args){
#            var uname=$('#{{obj.uname_element.input_element.tagid}}').val();
#            var upass=$('#{{obj.upass_element.input_element.tagid}}').val();
#            $.post("/", {action: 'login', username: uname, password: upass}, function(json){
#                if (!json){
#                    window.location.reload(true); 
#                }
#                else {
#                    data = $.parseJSON(json);
#                    $('#{{ obj.tagid }}').prepend(
#                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' + data.error + '</div>');
#                    window.setTimeout(function(){
#                        $(".alert").alert('close');
#                    }, 5000);
#                }
#            });
#        });
#    """)


class BootstrapApp(WebApp):

    stylesheets = ListProperty(
        ['static/css/bootstrap/bootstrap.min.css',
         # 'static/css/bootstrap/bootstrap-reboot.min.css',
         'static/css/bootstrap/fonts/font-awesome.min.css',
         'static/css/styles.css',
         ]
    )

    scripts = ListProperty(
        [
            'static/js/jquery.min.js',
            'static/js/bootstrap/bootstrap.bundle.min.js',
            'static/js/moho.js',
        ])

    def build_login(self):
        body = Body()
 #       body.add_element(BSLoginForm())
        return body
