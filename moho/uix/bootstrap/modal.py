"""
Modal
======================

"""
from moho.core.properties import StringProperty, ObjectProperty
from moho.core.attributes import ElementAttribute, ElementLogicalAttribute
from moho.uix.bootstrap.element import BootstrapElement


class ModalHeader(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')
    
    label_tagid = StringProperty('')
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %} >'
        '<h5 class="modal-title"  id="{{obj.label_tagid}}">{{ obj.text }}</h5>'
        '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">'
        '</button>'
        '{%raw obj.content%}</{{obj.tagname}}>'
    )    


class ModalBody(BootstrapElement):
    
    tagname = "div"
    
    
class ModalFooter(BootstrapElement):
    
    tagname = "div"
    
    
class Modal(BootstrapElement):
    
    tagname = "div"
    
    js_tpl = StringProperty("""

    var modal_{{obj.tagid}} = bootstrap.Modal.getOrCreateInstance(document.querySelector('#{{obj.tagid}}'));
    
    $('#{{obj.tagid}}').on("on_show", null, function(e, args){
        modal_{{obj.tagid}}.show();
    });
    
    $('#{{obj.tagid}}').on("on_toggle", null, function(e, args){
        modal_{{obj.tagid}}.toggle();
    });
    
    $('#{{obj.tagid}}').on("on_hide", null, function(e, args){
        modal_{{obj.tagid}}.hide();
    });
    
    """)
    
    header_text = StringProperty('')
    
    backdrop = ElementAttribute(None, options=["static"], attrname="data-bs-backdrop")
    
    keyboard = ElementLogicalAttribute(None, options=["true", "false"], attrname="data-bs-keyboard")
    
    header = ObjectProperty(None) 
        
    __accepted_children_types = [ModalHeader, ModalFooter, ModalBody]
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %}'
        'aria-labelledby="{{obj.header.label_tagid}}"  aria-hidden="true">'
        '<div class="modal-dialog" role="document">'
        '<div class="modal-content">'
        '{%raw obj.content%}'
        '</div></div>'
        '</{{obj.tagname}}>'
    )  
    
