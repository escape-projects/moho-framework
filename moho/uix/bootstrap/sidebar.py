"""
Sidebar
======================

"""
from moho.core.properties import StringProperty, ObjectProperty
from moho.core.attributes import ElementAttribute, ElementLogicalAttribute
from moho.core.attributes import ClassAttribute
from moho.uix.bootstrap.element import BootstrapElement


class SidebarHeader(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')
    
    label_tagid = StringProperty('')
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %} >'
        '<h5 class="offcanvas-title"  id="{{obj.label_tagid}}">{{ obj.text }}</h5>'
        '<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close">'
        '</button>'
        '{%raw obj.content%}</{{obj.tagname}}>'
    )    


class SidebarBody(BootstrapElement):
    
    tagname = "div"
    
    
class Sidebar(BootstrapElement):
    
    tagname = "div"
    
    js_tpl = StringProperty("""

    var sidebar_{{obj.tagid}} = bootstrap.Offcanvas.getOrCreateInstance(document.querySelector('#{{obj.tagid}}'));
    
    $('#{{obj.tagid}}').on("on_show", null, function(e, args){
        sidebar_{{obj.tagid}}.show();
    });
    
    $('#{{obj.tagid}}').on("on_toggle", null, function(e, args){
        sidebar_{{obj.tagid}}.toggle();
    });
    
    $('#{{obj.tagid}}').on("on_hide", null, function(e, args){
        sidebar_{{obj.tagid}}.hide();
    });
    
    """)
    
    header_text = StringProperty('')
    
    backdrop = ElementAttribute(None, options=["true", "false"], attrname="data-bs-backdrop")
    
    keyboard = ElementLogicalAttribute(None, options=["true", "false"], attrname="data-bs-keyboard")
    
    scroll = ElementLogicalAttribute(None, options=["true", "false"], attrname="data-bs-scroll")
    
    placement = ClassAttribute("start", options=["start", "end", "top", "bottom"],
                                    values=["offcanvas-start", "offcanvas-end", "offcanvas-top", "offcanvas-bottom"])
    
    header = ObjectProperty(None) 
        
    __accepted_children_types = [SidebarHeader, SidebarBody]
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %}'
        'aria-labelledby="{{obj.header.label_tagid}}">'
        '{%raw obj.content%}'
        '</{{obj.tagname}}>'
    )  
    
