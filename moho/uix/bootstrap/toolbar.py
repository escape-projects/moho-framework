"""
Toolbar
======================

"""
from moho.core.properties import OptionProperty,\
 NumericProperty, BoundedNumericProperty
from moho.uix.bootstrap.button import Button
from moho.uix.bootstrap.boxlayout import HBoxLayout
from moho.uix.bootstrap.dropdown import Dropdown

def _add_px(val):
    if val is not None and isinstance(val, (float, int)):
        val = f"{val}px"
    return val


class Toolbar(HBoxLayout):
 
    btn_size = OptionProperty(
         None, options=["small", "large"], allownone=True)
 
    btn_style = OptionProperty("primary", options=[
        "primary",
        "secondary",
        "success",
        "info",
        "warning",
        "danger",
        "dark",
        "light",
        "link",
        "outline-primary",
        "outline-secondary",
        "outline-success",
        "outline-info",
        "outline-warning",
        "outline-danger",
        "outline-dark",
        "outline-light",
        "outline-link",
        ], allownone=True
    )
    
    btn_spacing = BoundedNumericProperty(0, min=0, max=5)
    
    btn_height = NumericProperty(None, allownone=True)
    btn_width = NumericProperty(None, allownone=True)
    
    
    def add_element(self, element, index=0):
        if not isinstance(element, (Button, Dropdown)):
            raise TypeError(
                "only elements of type Button can be added to Toolbar")
        if isinstance(element, Button):
            element.btn_size = self.btn_size
            element.btn_style = self.btn_style
        
            h = _add_px(self.btn_height)
            w = _add_px(self.btn_width)
            
            element.min_height = h
            element.max_height = h
            
            element.min_width = w
            element.max_width = w
            
        if isinstance(element, Dropdown):
            element.btn_size = self.btn_size
            element.btn_style = self.btn_style
            h = _add_px(self.btn_height)
            element.min_height = h
            element.max_height = h
            
            
            
        self._update_element_margins(
            element, self.direction, self.btn_spacing)
            
        super(Toolbar, self).add_element(element, index=index)
    

    def on_btn_size(self, obj, val):
        for ch in self.children[:]:
            ch.btn_size = val
 
    def on_btn_style(self, obj, val):
        for ch in self.children[:]:
            if isinstance(ch, Button):
                ch.btn_style = val
                
    def on_btn_height(self, obj, val):
        val = _add_px(val)
        for ch in self.children[:]:
            ch.min_height = val
            ch.max_height = val
            
    def on_btn_width(self, obj, val):
        val = _add_px(val)
        for ch in self.children[:]:
            if isinstance(ch, Button):
                ch.min_width = val
                ch.max_width = val  
    
    def _update_element_margins(self, el, ori, sp):
        if ori=="column":
            el.mt = str(int(sp))
            el.mr = "0"
        else:
            el.mr = str(int(sp))
            el.mt = "0"
            
    def on_orientation(self, obj, val):
        for ch in self.children[:]:
            self._update_element_margins(
                ch, ori=val, sp=self.btn_spacing)
            
    def on_btn_spacing(self, obj, val):
        for ch in self.children[:]:
            self._update_element_margins(
                ch, ori = self.direction, sp=val)
        