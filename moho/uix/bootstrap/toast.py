"""
Toast
======================

"""
from moho.core.properties import StringProperty, BooleanProperty
from moho.uix.bootstrap.element import BootstrapElement


class ToastHeader(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')
    
    small_text = StringProperty('')
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %} >'
        '<strong class="me-auto">{{ obj.text }}</strong>'
        '<small>{{ obj.small_text }}</small>'
        '{%raw obj.content%}'
        '<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close">'
        '</button>'
        '</{{obj.tagname}}>'
    )    


class ToastBody(BootstrapElement):
    
    tagname = "div"
    
    
class Toast(BootstrapElement):
    
    tagname = "div"
    
    js_tpl = StringProperty("""

    var toast_{{obj.tagid}} = bootstrap.Toast.getOrCreateInstance(document.querySelector(
    '#{{obj.tagid}}'){% if not obj.autohide %}, options={autohide: false} {% end %}); 
    
    $('#{{obj.tagid}}').on("on_show", null, function(e, args){
        toast_{{obj.tagid}}.show();
    });
    
    $('#{{obj.tagid}}').on("on_dispose", null, function(e, args){
        toast_{{obj.tagid}}.dispose();
    });
    
    $('#{{obj.tagid}}').on("on_hide", null, function(e, args){
        toast_{{obj.tagid}}.hide();
    });
    
    """)
    
    autohide = BooleanProperty(False)
    
    header_text = StringProperty('')
    
    header_small_text = StringProperty('')
    
    # placement = ClassAttribute("start", options=["start", "end", "top", "bottom"],
    #                                values=["offcanvas-start", "offcanvas-end", "offcanvas-top", "offcanvas-bottom"])
        
    __accepted_children_types = [ToastHeader, ToastBody]
    
    html_tpl = StringProperty(
        '<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">'
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %}'
        'aria-atomic="true" role="alert" aria-live="assertive">'
        '{%raw obj.content%}'
        '</{{obj.tagname}}>'
        '</div>'
    )  
    
