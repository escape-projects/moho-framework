"""
LineEdit
======================

"""
from moho.core.properties import OptionProperty, NumericProperty
from moho.uix.bootstrap.input import InputField


class BaseLineEdit(InputField):
    
    type = OptionProperty("text", options=[
        "text", "number", "password", "datetime-local",
        "date", "month", "time",
        "week", "email",
        "url", "search", "tel", "color"]
    )

    size = OptionProperty(
        None, options=["large", "small"],
        allownone=True)
    
    min = NumericProperty(None, allownone=True)
    
    max = NumericProperty(None, allownone=True)
    
    step = NumericProperty(None, allownone=True)
    
    maxlength = NumericProperty(None, allownone=True)
    

class LineEdit(BaseLineEdit):
    pass

class FloatingLineEdit(BaseLineEdit):
    pass