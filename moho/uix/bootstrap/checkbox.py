"""
Checkbox
======================

"""

from moho.core.properties import (
    ObjectProperty, StringProperty,
    BooleanProperty
)
from moho.uix.bootstrap.element import BootstrapElement


class Checkbox(BootstrapElement):
    """
    Checkbox class, represents html <input type="Checkbox"/> element
    """

    tagname = 'div'
    
    input_element = ObjectProperty(None)
    
    label = StringProperty('')
    
    hint = StringProperty('')
    
    checked = BooleanProperty(False)
    
    disabled = BooleanProperty(False)

