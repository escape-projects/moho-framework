"""
Radio
======================

"""

from moho.core.properties import (
    ObjectProperty, StringProperty,
    BooleanProperty, NumericProperty
)
from moho.uix.bootstrap.element import BootstrapElement

                
class Radio(BootstrapElement):
    """
    Radio class, represents html <input type="radio"/> element
    """
    
    tagname = 'div'
    
    input_element = ObjectProperty(None)
    
    label = StringProperty('')
    
    disabled = BooleanProperty(False)
    
    checked = BooleanProperty(False)
    
    name = StringProperty('')
    
    
class RadioGroup(BootstrapElement):
    
    tagname = 'div'
    
    checked = NumericProperty(-1)
    
    def add_element(self, element, index=0):
        if not isinstance(element, Radio):
            raise TypeError("RadioGroup accepts children of type 'Radio' only")
        element.bind(checked=self.update_status)
        element.name = self.tagid
        if element.checked:
            self.checked = index
        BootstrapElement.add_element(self, element, index=index)
    
    def remove_element(self, element):
        element.unbind(checked=self.update_status)
        return BootstrapElement.remove_element(self, element)
    
    def update_status(self, obj, val):
        if val:
            idx = self.children.index(obj)
            self.checked = idx 
            for ch in self.children:
                if ch is not obj:
                    ch.checked = not val

    def on_checked(self, obj, val):
        self.children[val].checked = True
        
        
