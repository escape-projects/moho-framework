"""
Errorbar plot 
=============

Example:

.. code-block:: mh
    
    MplFigure:
        sin_x: np.linspace(0, 100, 300)
        sin_y: np.sin(2*np.pi*100*self.sin_x)
        MplCanvas:
            ErrorbarPlot:
                title: "Errorbar example"
                index: 3
                xlabel: "X"
                ylabel: "Y"
                ErrorbarLine:
                    label: "sin(x)"
                    x: root.sin_x
                    y: root.sin_y
                    yerr: np.sqrt(self.y)
                    
"""

from moho.core.properties import (ObjectProperty, 
StringProperty, ListProperty)

from .figure import SubPlot
from moho.uix.element import Element


class ErrorbarLine(Element):
    """
    Container class for the data arrays for single line.
    """
    
    parent = ObjectProperty(None)
    
    x = ObjectProperty([])
    """
    Specifies the x-coordinates.
    """
    y = ObjectProperty([])
    """
    Specifies the y-coordinates.
    """
    
    xerr = ObjectProperty(None, allownone=True)
    """
    Specifies the errors along x-coordinate.
    """
    
    yerr = ObjectProperty(None, allownone=True)
    """
    Specifies the errors along y-coordinate.
    """
    
    fmt = StringProperty("-")
    """
    Specifies the line format. A format string, e.g. 'ro' for red circles. See the Notes section for a full description
    of the format strings `here <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html>`_
    """
    
    label = StringProperty("")
    """
    Specifies the label for the line.
    """
    
    def draw(self, figid, ax):
        p = self.parent
        if p is None:
            return
        ax.errorbar(
            x=self.x, y=self.y, xerr=self.xerr, 
            yerr=self.yerr, label=self.label)
                  
    def remove(self, figid):
        pass
        
    def clear(self, figid):
        pass
        

class ErrorbarPlot(SubPlot):
    """
    Subplot element class for the errorbars. 
    """

    lines = ListProperty([])
    
    def draw_axes(self, figid, ax):
        super(ErrorbarPlot, self).draw_axes(figid, ax)
        for l in self.lines:
            l.parent = self
            l.draw(figid, ax)
        ax.legend(framealpha=0.7)
                
    def remove_axes(self, figid):
        super(ErrorbarPlot, self).remove_axes(figid)
        for l in self.lines:
            l.remove(figid)
            
    def clear_axes(self, figid, mgr):
        ax = super(ErrorbarPlot, self).clear_axes(figid, mgr)
        for l in self.lines:
            l.clear(figid)
        return ax
    
    def add_element(self, element, index=0):
        if isinstance(element, ErrorbarLine):
            self.lines.append(element)
        else:
            super(ErrorbarPlot, self).add_element(element, index=index)
    
    def add_elements(self, elements, index=0):
        if all([isinstance(el, ErrorbarLine) for el in elements]):
            self.lines.extend(elements)
        else:
            super(ErrorbarPlot, self).add_elements(elements, index=index)  
            
    def remove_element(self, element):
        if isinstance(element, ErrorbarLine):
            self.lines.remove(element)
        else:
            super(ErrorbarPlot, self).remove_element(element)  

