"""
Figure
=======

This module contains the main classes of plot components with support for different types of charts.

"""

import json
from tornado.websocket import WebSocketHandler

import matplotlib.pyplot as plt
from matplotlib.backends.backend_webagg_core import FigureManagerWebAgg, \
    NavigationToolbar2WebAgg
from matplotlib.backends.backend_webagg import FigureCanvasWebAgg
from matplotlib.figure import Figure

from moho.core.properties import (ObjectProperty,
StringProperty, BoundedNumericProperty,
BooleanProperty, ListProperty, OptionProperty)

from moho.uix.bootstrap.boxlayout import HBoxLayout, VBoxLayout

from moho.core.registry import element_registry

from weakref import WeakValueDictionary
import time
from matplotlib import _pylab_helpers, backend_bases
from moho.uix.bootstrap.toolbar import Toolbar
from moho.uix.element import Element
from moho.core.attributes import ElementNumericAttribute
from moho.uix.bootstrap.element import BootstrapElement


class MplWebSocket(WebSocketHandler):
    """
    Customized websocket handler class. The plot images are sent to clients as `png` images using the functionality of this handler.
    """
    
    alias = 'mpl'

    supports_binary = True
    
    def __init__(self, *args, **kwargs):
        super(MplWebSocket, self).__init__(*args, **kwargs)
        self.el = None
        self.manager = None
        self.figid = None
    
    def open(self, tagid, figid):
        el = element_registry.get(int(tagid[2:]), None)
        if el is None:
            raise ValueError("No element with tagid '{0}' found".format(tagid))
        figid = int(float(figid))
        manager = el.init_figure(figid)
        manager.add_web_socket(self)
        self.manager = manager
        self.el = el
        self.figid = figid
        if hasattr(self, 'set_nodelay'):
            self.set_nodelay(True)

    def on_close(self):
        el = self.el
        mgr = self.manager
        if mgr is not None:
            mgr.remove_web_socket(self)
        if el is not None:
            el.close_figure(self.figid)

    def on_message(self, message):
        message = json.loads(message)
        # The 'supports_binary' message is on a client-by-client
        # basis.  The others affect the (shared) canvas as a
        # whole.
        if message['type'] == 'supports_binary':
            self.supports_binary = message['value']
        else:
            manager = self.manager
            # It is possible for a figure to be closed,
            # but a stale figure UI is still sending messages
            # from the browser.
            if manager is not None:
                manager.handle_json(message)

    def send_json(self, content):
        self.write_message(json.dumps(content))

    def send_binary(self, blob):
        if self.supports_binary:
            self.write_message(blob, binary=True)
        else:
            data_uri = "data:image/png;base64,{0}".format(
                blob.encode('base64').replace('\n', ''))
            self.write_message(data_uri)


_ESCAPE_ICON_CLASSES = {
    'home': 'fa-home',
    'back': 'fa-arrow-left',
    'forward': 'fa-arrow-right',
    'zoom_to_rect': 'fa-search-plus',
    'move': 'fa-arrows-alt',
    # 'download': 'ui-icon ui-icon-disk',
    None: None,
}


class MplNavigationToolbar(NavigationToolbar2WebAgg):

    # Use the standard toolbar items + download button
    toolitems = [(text, tooltip_text, _ESCAPE_ICON_CLASSES[image_file],
                  name_of_method)
                 for text, tooltip_text, image_file, name_of_method
                 in backend_bases.NavigationToolbar2.toolitems
                 if image_file in _ESCAPE_ICON_CLASSES]


class MplFigureManagerWebAgg(FigureManagerWebAgg):
    ToolbarCls = MplNavigationToolbar


class SubPlot(Element):
    """
    Represents subplot element. Each `MplCanvas` component can manage several subplots which are positioned in a grid.
    """
    
    scale_options = ["linear", "log", "symlog", "logit"]
    
    parent = ObjectProperty(None)
    
    index = BoundedNumericProperty(1, min=1, max=100)
    """
    Specifies index of the subplot in the grid.
    """
    
    title = StringProperty("")
    """
    Specifies title.
    """
    
    xlabel = StringProperty("X")
    """
    Specifies label for x-axis.
    """
    
    ylabel = StringProperty("Y")
    """
    Specifies label for y-axis.
    """
    
    xscale = OptionProperty("linear", options=scale_options)
    """
    Specifies scale for x-coordinates. Possible values are `linear`, `log`, `symlog`, `logit`.
    """
    
    yscale = OptionProperty("linear", options=scale_options)
    """
    Specifies scale for y-coordinates. Possible values are `linear`, `log`, `symlog`, `logit`.
    """
    
    def __init__(self, *args, **kwargs):
        self.mpl_axes = WeakValueDictionary()
        super(SubPlot, self).__init__(*args, **kwargs)
    
    def remove_axes(self, figid):
        del self.mpl_axes[figid]
        
    def axes_relim(self):
        for ax in self.mpl_axes.values():
            ax.relim()
            ax.autoscale_view(True, True, True)
    
    def initialize(self, parent, figid, mgr):
        p = parent
        self.parent = p
        fig = mgr.canvas.figure
        ax = fig.add_subplot(p.nrows, p.ncols, self.index)
        self.mpl_axes[figid] = ax
        self.draw_axes(figid, ax)
        
    def draw_axes(self, figid, ax):
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_title(self.title)
        ax.set_xscale(self.xscale)
        ax.set_yscale(self.yscale)
        
    def clear_axes(self, figid, mgr):
        mplax = self.mpl_axes[figid]
        mplax.clear()
            
    def on_xlabel(self, obj, val):
        for ax in self.mpl_axes.values():
            ax.set_xlabel(val)
            
    def on_ylabel(self, obj, val):
        for ax in self.mpl_axes.values():
            ax.set_ylabel(val)
            
    def on_title(self, obj, val):
        for ax in self.mpl_axes.values():
            ax.set_title(val) 
            
    def on_xscale(self, obj, val):
        for ax in self.mpl_axes.values():
            ax.set_xscale(val)
            
    def on_yscale(self, obj, val):
        for ax in self.mpl_axes.values():
            ax.set_yscale(val)
        
    def update(self, figid, mgr):
        for mplax in self.mpl_axes.values():
            self.clear_axes(figid, mgr)
            self.draw_axes(figid, mplax)
    

class Canvas(BootstrapElement):
    
    tagname = "canvas"
    
    height_attr = ElementNumericAttribute(
        "100px", attrname="height")
    
    width_attr = ElementNumericAttribute(
        "100px", attrname="width")


class MplCanvas(HBoxLayout):
    """
    Represents component which handles canvases for plot image and mouse operations and is a holder of the figure subplots.
    """
    
    canvas = ObjectProperty(None)
    rubberband_canvas = ObjectProperty(None)
    
    dpi = BoundedNumericProperty(72, min=15, max=300)
    """
    Specifies the DPI resolution for the plot image.
    """
    
    facecolor = StringProperty(None)
    """
    Specifies facecolor of the plot.
    """
    
    edgecolor = StringProperty(None)
    """
    Specifies edgecolor of the plot.
    """
    
    frameon = BooleanProperty(False)
    """
    If `False`, suppress drawing the figure background patch.
    """
    
    tight_layout = BooleanProperty(False)
    """
    Specifies if tight layout is used.
    """
    
    nrows = BoundedNumericProperty(1, min=1, max=100)
    """
    Specifies number of rows in the figure grid.
    """
    
    ncols = BoundedNumericProperty(1, min=1, max=100)
    """
    Specifies number of columns in the figure grid.
    """
    
    plot_style = OptionProperty(
            "default", options=['default', 'classic'] + sorted(
                style for style in plt.style.available if style != 'classic'))
    """
    Specifies style of the plot. Please find more information about available styles `here <https://matplotlib.org/stable/tutorials/introductory/customizing.html>`_
    """
    
    subplots = ListProperty([])
    
    def __init__(self, *args, **kwargs):
        super(MplCanvas, self).__init__(*args, **kwargs)
        self.managers = WeakValueDictionary()
        
        canvas = Canvas(width="100%", height="100%")
        rubberband_canvas = Canvas(
            width="100%",
            height="100%",
            css_declarations={
                "position": "absolute",
                "left": "0",
                "top": "0",
                "z-index": "1"
            }
        )
        
        self.add_element(canvas)
        self.add_element(rubberband_canvas)
        
        self.canvas = canvas
        self.rubberband_canvas = rubberband_canvas
        
    def init_figure(self, figid):
        """Create a new instance of figure manager"""
        with plt.style.context(self.plot_style):
            fig_cls = Figure
            fig = fig_cls(dpi=self.dpi,
                          facecolor=self.facecolor,
                          edgecolor=self.edgecolor,
                          frameon=self.frameon,
                          tight_layout=self.tight_layout)
            canvas = FigureCanvasWebAgg(fig)
            mgr = MplFigureManagerWebAgg(canvas, figid)
            self.managers[figid] = mgr
            for spl in self.subplots:
                spl.initialize(self, figid, mgr)
            canvas.draw_idle()
            return mgr
            
    def close_figure(self, figid):
        mgr = self.managers.get(figid, None)
        if mgr is not None:
            fig = mgr.canvas.figure
            _pylab_helpers.Gcf.destroy_fig(fig)
            del self.managers[figid]
            for spl in self.subplots:
                spl.remove_axes(figid)
    
    def update_subplot(self, idx, *args):
        if not isinstance(idx, (list, tuple)):
            idx = [idx]
        for figid, mgr in self.managers.items():
            for i in idx:
                s = self.subplots[i] 
                s.update(figid, mgr)
            mgr.canvas.draw_idle()
                   
    def update_figure(self, *args):
        for figid, mgr in self.managers.items():
            for spl in self.subplots: 
                spl.update(figid, mgr)
            mgr.canvas.draw_idle()
            
    def add_element(self, element, index=0):
        if isinstance(element, SubPlot):
            self.subplots.append(element)
        elif isinstance(element, Canvas):
            super(MplCanvas, self).add_element(element, index)
        else:
            print(element)
            raise TypeError("{0} supports elements of type {1} only".format(self.__class__.__name__, SubPlot.__name__))
    
    def add_elements(self, element, index=0):
        if all([isinstance(el, SubPlot) for el in element]):
            self.subplots.extend(element)
        elif isinstance(element, Canvas):
            super(MplCanvas, self).add_elements(element, index)
        else:
            raise TypeError("{0} supports elements of type {1} only".format(self.__class__.__name__, SubPlot.__name__))

        
class MplToolbar(Toolbar):
    """
    Represents toolbar of the figure.
    """
    
    toolbar_items = ListProperty(MplFigureManagerWebAgg.ToolbarCls.toolitems)


class MplStatusbar(HBoxLayout):
    """
    Represents statusbar of the figure.
    """
    

class MplFigure(VBoxLayout):
    """
    Represents component which is a layout for figure canvas, toolbar and statusbar.
    """
    
    websocket_cls = ObjectProperty(MplWebSocket)
    
    websocket_host = StringProperty("/ws/mpl/([^/]*)/([^/]*)")
    
    js_tpl = StringProperty("""
     
    {% set figid = obj.gen_fig_id() %}
     function ready(fn) {
        if (document.readyState != "loading") {
          fn();
        } else {
          document.addEventListener("DOMContentLoaded", fn);
        }
      }
       
      ready(
        function () {
          var url = ((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.host + "/ws/mpl/"+ "{{ str(obj.tagid) }}" + '/' + "{{ str(figid) }}";
          console.log(url);
          var toolbar_items = {% raw json_encode(obj.toolbar_items) %};
          var websocket_type = mpl.get_websocket_type();
          var websocket = new websocket_type(url);
          var fig = new mpl.figure(
              "{{ str(figid) }}", websocket, toolbar_items, "{{obj.canvas_layout_id}}", "{{obj.toolbar_id}}", "{{obj.statusbar_id}}",
              document.getElementById("{{obj.tagid}}"), "{{obj.toolbar_style}}");
        }
      );
    """)
        
    toolbar = ObjectProperty(None)
    canvas_layout = ObjectProperty(None)
    statusbar = ObjectProperty(None)
    
    script = StringProperty("static/js/mpl.js")
    """
    Specifies path to the javascript file which is required for operation of client.
    """
        
    def gen_fig_id(self):
        return str(time.time())
       
    def add_element(self, element, index=0):
        if isinstance(element, MplToolbar):
            self.toolbar = element
        elif isinstance(element, MplCanvas):
            self.canvas_layout = element
        elif isinstance(element, MplStatusbar):
            self.statusbar = element   
        super(MplFigure, self).add_element(element, index=index)
        
    def remove_element(self, element):
        if isinstance(element, (MplToolbar, MplCanvas, MplStatusbar)):
            raise TypeError("Removal of figure toolbar, statusbar or canvas is not supported")
        else: 
            super(MplFigure, self).remove_element(element)
 
    def init_figure(self, figid):
        cl = self.canvas_layout
        if cl is None:
            raise ValueError("Canvas layout object doesn't exist")
        return cl.init_figure(figid)
            
    def close_figure(self, figid):
        cl = self.canvas_layout
        cl.close_figure(figid)
 
    @property
    def toolbar_items(self):
        if self.toolbar is not None:
            return self.toolbar.toolbar_items
        return []
    
    @property
    def toolbar_style(self):
        if self.toolbar is not None:
            return self.toolbar.btn_style
        return ''
    
    @property
    def toolbar_id(self):
        if self.toolbar is not None:
            return self.toolbar.tagid
        return ''
    
    @property
    def statusbar_id(self):
        if self.statusbar is not None:
            return self.statusbar.tagid
        return ''
    
    @property
    def canvas_layout_id(self):
        if self.canvas_layout is not None:
            return self.canvas_layout.tagid
        return ''

    def update_subplot(self, idx, *args):
        cl = self.canvas_layout
        cl.update_subplot(idx)
                   
    def update_figure(self, *args):
        cl = self.canvas_layout
        cl.update_figure()
            
