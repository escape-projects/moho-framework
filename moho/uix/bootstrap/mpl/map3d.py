"""
Map3d plot 
============

Example:

.. code-block:: mh
    
    MplFigure:
        x: np.linspace(0, 100, 300)
        y: np.linspace(0, 100, 300)
        xy_grid: np.meshgrid(self.x, self.y)
        mu_x: 49
        mu_y: 49
        sigma: 1
        ampl: 10
        map_z: np.exp(-(self.xy_grid[0]-self.mu_x)**2/(2*self.sigma**2))*np.exp(-(self.xy_grid[1]-self.mu_y)**2/(2*self.sigma**2))*self.ampl
        MplCanvas:
            index: 1
            Map3d:
                title: "Gaussian"
                xlabel: "X"
                ylabel: "Y"
                z: root.map_z
                    

"""


import numpy as np
from weakref import WeakValueDictionary

from moho.core.properties import (
    ObjectProperty, NumericProperty, OptionProperty, ListProperty)
from .figure import SubPlot



class Map3d(SubPlot):
    """
    Subplot element class for the image (map3d) plot. 
    """
    cmap = OptionProperty('viridis',
                           options = [
                               'viridis', 'plasma',
                               'inferno', 'magma',
                               'cividis'
                            ])
    """
    Specifies color mapping.
    """
    
    z = ObjectProperty(np.zeros(shape=(10, 10)), force_dispatch=True)
    """
    Specifies z-axis data. It should be a two-dimensional array.
    """
    
    aspect = NumericProperty(None)
    """
    Specifies the aspect ratio of the image.
    """
    
    interpolation = OptionProperty('none',
                           options = [
                               'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
                                'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
                                'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos'
                            ])
    """
    Specifies the interpolation method of the image.
    """
    
    origin = OptionProperty('lower',
                           options = [
                               'lower', 'upper'
                            ])
    
    extent = ListProperty([])
    """
    Specifies the axes extent of the image plot in the form [xmin, xmax, ymin, ymax].
    """
    
    def __init__(self, *args, **kwargs):
        self.mpl_images = WeakValueDictionary()
        self.mpl_cbars = WeakValueDictionary()
        super(Map3d, self).__init__(*args, **kwargs)
    
    def draw_axes(self, figid, ax):
        fig = self.parent.managers[figid].canvas.figure
        super(Map3d, self).draw_axes(figid, ax)
        im = ax.imshow(self.z, interpolation=self.interpolation, origin=self.origin,
                       cmap=self.cmap,
                       aspect='auto' if self.aspect is None else self.aspect)
        cb = fig.colorbar(im, ax=ax)
        self.mpl_images[figid] = im
        self.mpl_cbars[figid] = cb
        
    def remove_axes(self, figid):
        SubPlot.remove_axes(self, figid)
        del self.mpl_images[figid]
        del self.mpl_cbars[figid]
        
    def clear_axes(self, figid, mgr):
        ax = SubPlot.clear_axes(self, figid, mgr)
        del self.mpl_images[figid]
        cb = self.mpl_cbars[figid]
        cb.remove()
        del self.mpl_cbars[figid]
        return ax
        
    def on_z(self, obj, val):
        extent = self.extent or (0, val.shape[1], 0, val.shape[0])
        le = len(extent)
        if le<4:
            raise ValueError(f"Wrong length of extent layer, expected 4, got {le}")  
        for im in self.mpl_images.values():
            im.set_data(np.copy(val))
            im.set_clim(vmin=np.min(val), vmax=np.max(val))
            im.set_extent(extent)
        p = self.parent
        if p is not None:
            self.axes_relim()
    
    def update(self, figid, mgr):
        pass
