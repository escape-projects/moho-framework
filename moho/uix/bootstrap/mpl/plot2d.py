"""
2D plot 
============
"""

from moho.core.properties import (ObjectProperty, 
StringProperty, ListProperty)

from moho.core.event import EventDispatcher
from weakref import WeakValueDictionary
from .figure import SubPlot
from moho.uix.element import Element


class Line(Element):
    
    parent = ObjectProperty(None)
    
    x = ObjectProperty([])
    y = ObjectProperty([])
    
    fmt = StringProperty("-")
    
    label = StringProperty("")
    
    def __init__(self, *args, **kwargs):
        self.mpl_lines = WeakValueDictionary()
        super(Line, self).__init__(*args, **kwargs)
        
    def draw(self, figid, ax):
        p = self.parent
        if p is None:
            return
        x = self.x
        y = self.y
        l = None
        if x is None and y is None: 
            l = ax.plot([], [], self.fmt, label=self.label)[0]
        elif x is None and y is not None:
            l = ax.plot(y, self.fmt, label=self.label)[0]
        else:
            l = ax.plot(x, y, self.fmt, label=self.label)[0]
        if l is not None:
            self.mpl_lines[figid]=l
                  
    def on_x(self, obj, val):
        for l in self.mpl_lines.values():
            l.set_xdata(val)
            
            
    def on_y(self, obj, val):
        for l in self.mpl_lines.values():
            l.set_ydata(val)
       
    def remove(self, figid):
        del self.mpl_lines[figid]
        
    def clear(self, figid):
        del self.mpl_lines[figid]
        
    def on_label(self, obj, val):
        for l in self.mpl_lines.values():
            l.set_label(val)
        

class Plot2d(SubPlot):
    
    lines = ListProperty([])
    
    def draw_axes(self, figid, ax):
        super(Plot2d, self).draw_axes(figid, ax)
        for l in self.lines:
            l.parent = self
            l.draw(figid, ax)
        ax.legend(framealpha=0.7)
                
    def remove_axes(self, figid):
        SubPlot.remove_axes(self, figid)
        for l in self.lines:
            l.remove(figid)
            
    def clear_axes(self, figid, mgr):
        ax = SubPlot.clear_axes(self, figid, mgr)
        for l in self.lines:
            l.clear(figid)
        return ax
    
    def update(self, figid, mgr):
        self.axes_relim()
    
    def add_element(self, element, index=0):
        if isinstance(element, Line):
            self.lines.append(element)
        else:
            super(Plot2d, self).add_element(element, index=index)  
    
    def add_elements(self, elements, index=0):
        if all([isinstance(el, Line) for el in elements]):
            self.lines.extend(elements)
        else:
            super(Plot2d, self).add_elements(elements, index=index)  
            
    def remove_element(self, element):
        if isinstance(element, Line):
            self.lines.remove(element)
        else:
            super(Plot2d, self).remove_element(element)  
        





