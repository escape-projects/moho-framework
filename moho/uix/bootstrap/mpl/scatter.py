"""
Scatter plot 
============

Example:

.. code-block:: mh
    
    MplFigure:
        scatter_pos: np.asarray([np.random.randint(1000, size=(300)), np.random.randint(1000, size=(300))])
        scatter_s: np.random.randint(100, size=(300))
        scatter_c: np.random.randint(500, size=(300))
        MplCanvas:
            index: 1
                Scatter:
                    index: 1
                    title: "Scatter"
                    xlabel: "X"
                    ylabel: "Y"
                    pos: root.scatter_pos
                    s: root.scatter_s
                    c: root.scatter_c
"""

from weakref import WeakValueDictionary

from moho.core.properties import (
    ObjectProperty, BoundedNumericProperty, OptionProperty)
from .figure import SubPlot


class Scatter(SubPlot):
    """
    Scatter plot component. For full documentation please check `this page <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.scatter.html>_`.
    """
    
    alpha = BoundedNumericProperty(0.5, min=0, max=1)
    """
    Specifies transparency.
    """
    
    pos = ObjectProperty([[], []], force_dispatch=True)
    """
    Specifies coordinates of data points.
    """
    
    s = ObjectProperty([])
    """
    Specifies radius of data. points.
    """
    
    c = ObjectProperty([])
    """
    Specifies colors of data points.
    """
    
    cmap = OptionProperty('viridis',
                           options = [
                               'viridis', 'plasma',
                               'inferno', 'magma',
                               'cividis'
                            ])
    
    
    def __init__(self, *args, **kwargs):
        self.mpl_images = WeakValueDictionary()
        self.mpl_cbars = WeakValueDictionary()
        super(Scatter, self).__init__(*args, **kwargs)
    
    def draw_axes(self, figid, ax):
        fig = self.parent.managers[figid].canvas.figure
        super(Scatter, self).draw_axes(figid, ax)
        im = ax.scatter(self.pos[0], self.pos[1], s=self.s, c=self.c,
                       cmap=self.cmap, alpha=self.alpha)
        cb = fig.colorbar(im, ax=ax)
        self.mpl_images[figid] = im
        self.mpl_cbars[figid] = cb
        
    def remove_axes(self, figid):
        SubPlot.remove_axes(self, figid)
        del self.mpl_images[figid]
        cb = self.mpl_cbars[figid]
        cb.remove()
        del self.mpl_cbars[figid]
    
    def clear_axes(self, figid, mgr):
        ax = SubPlot.clear_axes(self, figid, mgr)
        del self.mpl_images[figid]
        cb = self.mpl_cbars[figid]
        cb.remove()
        del self.mpl_cbars[figid]
        return ax
    
    def update(self, figid, mgr):
        pass
    
    def on_pos(self, obj, val):
        for im in self.mpl_images.values():
            im.set_offsets(val)
            
        p = self.parent
        if p is not None:
            self.axes_relim()
            
    def on_s(self, obj, val):
        for im in self.mpl_images.values():
            im.set_sizes(val)
            
    def on_c(self, obj, val):
        for im in self.mpl_images.values():
            im.set_array(val)
        

