"""
Matplotlib 
============


This package contains elements and components with interface to `Matplotlib <https://matplotlib.org/>`_ python plotting library. 
Components presented in this package are a part of `moho.uix.bootstrap` and cannot be used without it.

Each figure component :class:`~moho.uix.bootstrap.mpl.figure.MplFigure` is a vertical box layout which can contain plot toolbar
:class:`~moho.uix.bootstrap.mpl.figure.MplToolbar`,
plot statusbar :class:`~moho.uix.bootstrap.mpl.figure.MplStatusbar`
and always has a component :class:`~moho.uix.bootstrap.mpl.figure.MplCanvas` which manages subplots.
The  plot image is built on the server side and is sent to browser by 
:class:`~moho.uix.bootstrap.mpl.figure.MplWebSocket` instance as a `png` image.
 
The `MplCanvas` component manages the `matplotlib.figure` instances allowing to have different
plot resolution or viewport (zoom window) for each active client . Unfortunately, this feature can slow down 
the application if plots are updated often (live displays) and there are too many connected clients.

Below is the example of figure component with two subplots, toolbar and statusbar:

.. code-block:: mh

    MplFigure:
        MplToolbar:
        MplCanvas:
            flex_basis: "80%"    
            tight_layout: True
            plot_style: 'seaborn'
            nrows: 1
            ncols: 2
            Plot2d:
                title: "Sine plot"
                index: 1
                xlabel: "t [sec]"
                ylabel: "f(t)"
                Line:
                    x: np.linspace(0, 100, 300)
                    y: np.sin(2*np.pi*1000*self.x)
                    label: "sin(x)"
            Plot2d:
                title: "Cosine plot"
                index: 2
                xlabel: "t [sec]"
                ylabel: "f(t)"
                Line:
                    x: np.linspace(0, 100, 300)
                    y: np.cos(2*np.pi*1000*self.x)
                    label: "cos(x)" 
                    
                    
`MplCanvas` supports currently the following types of subplots:
    
    
"""


