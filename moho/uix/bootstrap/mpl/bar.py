"""
Bar plot 
============

Example:

.. code-block:: mh
    
    MplFigure:
        MplCanvas:
            BarPlot:
                title: "Bars"
                xlabel: "X"
                ylabel: "Y"
                BarData:
                    label: "Men"
                    x: np.arange(5) + 0.35
                    height: np.array([20, 34, 30, 35, 27])
                    width: 0.35

"""

from moho.core.properties import (ObjectProperty, 
StringProperty, NumericProperty, ListProperty)

from .figure import SubPlot
from moho.uix.element import Element


class BarData(Element):
    """
    `BarData` is a container for the user data which is represented as a bar plot.
    """
    
    parent = ObjectProperty(None)
    
    x = ObjectProperty([])
    """
    Specifies the x-coordinates of the bars.
    """
    
    height = ObjectProperty([])
    """
    Specifies the heights of the bars.
    """
    
    width = NumericProperty(0.8)
    """
    Specifies the widths of the bars.
    """
    
    label = StringProperty("")
    """
    Specifies the label of the data.
    """
    
    def draw(self, figid, ax):
        p = self.parent
        if p is None:
            return
        ax.bar(
            x=self.x, height=self.height,
            width=self.width, label=self.label)
                  
    def remove(self, figid):
        pass
        
    def clear(self, figid):
        pass
        

class BarPlot(SubPlot):
    """
    Subplot element class for the bar plots. 
    """
    
    bars = ListProperty([])
    
    def draw_axes(self, figid, ax):
        super(BarPlot, self).draw_axes(figid, ax)
        for l in self.bars:
            l.parent = self
            l.draw(figid, ax)
        ax.legend(framealpha=0.7)
                
    def remove_axes(self, figid):
        super(BarPlot, self).remove_axes(figid)
        for l in self.bars:
            l.remove(figid)
            
    def clear_axes(self, figid, mgr):
        ax = super(BarPlot, self).clear_axes(figid, mgr)
        for l in self.bars:
            l.clear(figid)
        return ax
    
    def add_element(self, element, index=0):
        if isinstance(element, BarData):
            self.bars.append(element)
        else:
            super(BarPlot, self).add_element(element, index=index)  
    
    def add_elements(self, elements, index=0):
        if all([isinstance(el, BarData) for el in elements]):
            self.bars.extend(elements)
        else:
            super(BarPlot, self).add_elements(elements, index=index)  
            
    def remove_element(self, element):
        if isinstance(element, BarData):
            self.bars.remove(element)
        else:
            super(BarPlot, self).remove_element(element)  




