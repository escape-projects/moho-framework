"""
Card
======================

"""

from moho.core.properties import OptionProperty, StringProperty

from moho.core.attributes import ClassAttribute, ElementAttribute, \
    ElementNumericAttribute, EventAttribute
from moho.uix.bootstrap.boxlayout import BoxLayout
from moho.uix.element import NoContentHtmlElement
from moho.uix.bootstrap.element import BootstrapElement


class CardBody(BoxLayout):
    
    title = StringProperty('')
    
    subtitle = StringProperty('')
    
    title_tagname = OptionProperty(
        "h5",
        options=["h1", "h2", "h3", "h4", "h5", "h6", "div"])
    
    html_tpl = StringProperty("""
    <{{obj.tagname}} id='{{obj.tagid}}' {% if obj.cls %} class='{{obj.cls}}' {% end %} {% if obj.style %} style='{{obj.style}}' {% end %} {% raw obj.attributes %} >
         {% if obj.title %}
         <{{obj.title_tagname}} class="card-title">{{obj.title}}</{{obj.title_tagname}}> 
         {% end %}
         {% if obj.subtitle %}
         <h6 class="card-subtitle">{{obj.subtitle}}</h6> 
         {% end %}
         {%raw obj.content%}
    </{{obj.tagname}}>""")
    
    
class CardImage(NoContentHtmlElement):
    
    tagname = "img"

    src = ElementAttribute('', remote=True)
    """
    Specifies the path to the image
    """

    alt = ElementAttribute(None)
    """
    Specifies an alternate text for an image
    """

    height_attr = ElementNumericAttribute(
        None, attrname="height", remote=True)
    """Specifies the height of an image"""

    width_attr = ElementNumericAttribute(
        None, attrname="width", remote=True)
    """Specifies the width of an image"""

    onerror = EventAttribute(False)
    """Fires when error occurs"""

    onload = EventAttribute(False)
    """Fires after the image is finished loading"""
    
    type = ClassAttribute(
        "top",
        
        options=["top", "overlay"],
        
        values=["card-img-top", "card-img"]
    )


class Card(BootstrapElement):
    
    tagname = "div"
    
    header = StringProperty('')
    
    footer = StringProperty('')
    
    html_tpl = StringProperty("""
    <{{obj.tagname}} id='{{obj.tagid}}' {% if obj.cls %} class='{{obj.cls}}' {% end %} {% if obj.style %} style='{{obj.style}}' {% end %} {% raw obj.attributes %} >
         {% if obj.header %}
         <div class="card-header">{{obj.header}}</div> 
         {% end %}
         {%raw obj.content%}
         {% if obj.footer %}
         <div class="card-footer">{{obj.footer}}</div> 
         {% end %}
    </{{obj.tagname}}>""")
    
    text_color = ClassAttribute(
        None,
        options=["primary", "secondary", "success",
                 "danger", "warning", "info", "muted",
                 "light", "dark", "white"
                 ],
        values=["text-primary", "text-secondary",
                "text-success", "text-danger",
                "text-warning", "text-info",
                "text-muted", "text-light",
                "text-dark", "text-white"]
    )
    
    background_color = ClassAttribute(
        None,
        options=["primary", "secondary", "success",
                 "danger", "warning", "info",
                 "light", "dark", "white"
                 ],
        values=["bg-primary", "bg-secondary",
                "bg-success", "bg-danger",
                "bg-warning", "bg-info",
                "bg-light",
                "bg-dark", "bg-white"]
    )

    border_color = ClassAttribute(
        None,
        options=["primary", "secondary", "success",
                 "danger", "warning", "info",
                 "light", "dark"
                 ],
        values=["border-primary", "border-secondary",
                "border-success", "border-danger",
                "border-warning", "border-info",
                "border-light",
                "border-dark"]
    )
        
            
class CardDeck(BootstrapElement):
    
    tagname = "div"
    
    __accepted_children_types__ = [Card]
    
