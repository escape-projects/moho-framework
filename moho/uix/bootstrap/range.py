"""
Range
======================

"""

from moho.core.properties import (
    ObjectProperty, StringProperty,
    BooleanProperty, NumericProperty
)

from moho.uix.element import NoContentHtmlElement
from moho.core.attributes import ElementNumericAttribute
from moho.uix.html5.input import InputBase
from moho.uix.bootstrap.element import BootstrapElement


class RangeInput(InputBase, NoContentHtmlElement):
    
    tagname = "input"
    
    value = ElementNumericAttribute(0, remote=True)
    

class Range(BootstrapElement):
    
    tagname = 'div'
    
    input_element = ObjectProperty(None)
    
    label = StringProperty('')
    
    hint = StringProperty('')
    
    max = NumericProperty(10)
    
    min = NumericProperty(0)
    
    step = NumericProperty(0.1)
    
    value = NumericProperty(5)
    
    disabled = BooleanProperty(False)

