"""
Button
======================

"""

from moho.core.attributes import ClassAttribute, ElementAttribute, \
    ElementBooleanAttribute
from moho.core.properties import (
    StringProperty, ObjectProperty)
from moho.uix.bootstrap.spinner import GrowSpinner, Spinner
from moho.uix.html5.button import ButtonBase
from moho.uix.bootstrap.element import BootstrapElement


class Button(ButtonBase, BootstrapElement):
    
    tagname = "button"
    
    tooltip = StringProperty(None)
    
    popover = StringProperty(None)
    
    popover_title = StringProperty(None)
    
    title = ElementAttribute(None, remote=True)
        
    text = StringProperty('')
    
    icon = StringProperty(None)
    
    data_toggle = ElementAttribute(None, options=["tooltip", "popover"])
    
    data_content = ElementAttribute(None, remote=True)
    
    data_container = ElementAttribute(None)
    
    data_placement = ElementAttribute(
        None, options=['left', 'right', 'bottom', 'top'])
    
    btn_type = ElementAttribute(None, options=['button', 'submit', 'reset'])

    btn_style = ClassAttribute("primary", options=[
        "primary",
        "secondary",
        "success",
        "info",
        "warning",
        "danger",
        "dark",
        "light",
        "link",
        "outline-primary",
        "outline-secondary",
        "outline-success",
        "outline-info",
        "outline-warning",
        "outline-danger",
        "outline-dark",
        "outline-light",
        "outline-link",

        ],

        values=[
            "btn-primary",
        "btn-secondary",
        "btn-success",
        "btn-info",
        "btn-warning",
        "btn-danger",
        "btn-dark",
        "btn-light",
        "btn-link",
        "btn-outline-primary",
        "btn-outline-secondary",
        "btn-outline-success",
        "btn-outline-info",
        "btn-outline-warning",
        "btn-outline-danger",
        "btn-outline-dark",
        "btn-outline-light",
        "btn-outline-link",
        ], allownone=True
    )

    btn_size = ClassAttribute(None, options=["large", "small"],
                                 values=["btn-lg", "btn-sm"])
    
    mt = ClassAttribute("0",
                                options=["0", "1", "2", "3", "4", "5"],
                                values=["mt-0", "mt-1",
                                        "mt-2", "mt-3",
                                        "mt-4", "mt-5"])
    
    mr = ClassAttribute("0",
                                options=["0", "1", "2", "3", "4", "5"],
                                values=["mr-0", "mr-1",
                                        "mr-2", "mr-3",
                                        "mr-4", "mr-5"])
    
    spinner = ObjectProperty(None, allownone=True)
    
    def __init__(self, **kwargs):
        super(Button, self).__init__(**kwargs)
        self.bind(text=self.update_content)
        self.bind(icon=self.update_content)
        self.update_content()
    
    def update_content(self, *args, **kwargs):
        ic = self.icon
        txt = self.text
        cnt = txt
        if ic:
            cnt = '<i class="fa fa-{0}"></i>&nbsp;{1}'.format(ic, txt)
        self.content = cnt          
        
    def add_element(self, element, index=0):
        if isinstance(element, (Spinner, GrowSpinner)):
            self.spinner = element
        super(Button, self).add_element(element, index=index)
        
    def remove_element(self, element):
        super(Button, self).remove_element(element)
        sp = self.spinner
        if sp is element:
            sp = None
    
