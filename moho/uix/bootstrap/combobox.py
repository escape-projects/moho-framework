"""
Combobox
======================

"""

from moho.core.properties import OptionProperty, ListProperty, \
BooleanProperty, ObjectProperty, StringProperty

from moho.core.attributes import ClassAttribute
from moho.uix.html5.select import SelectBase, Option
from moho.uix.bootstrap.element import BootstrapElement


class Select(SelectBase, BootstrapElement):

    tagname = "select"

    size_cls = ClassAttribute(
        None, options=["large", "small"],
        values=["form-control-lg", "form-control-sm"])


class BaseCombobox(BootstrapElement):

    tagname = 'div'

    input_element = ObjectProperty(None)

    label = StringProperty('')

    hint = StringProperty('')

    checked = BooleanProperty(False)

    disabled = BooleanProperty(False)

    size = OptionProperty(
        None, options=["large", "small"],
        allownone=True)

    values = ListProperty([])

    value = StringProperty()

    multiple = BooleanProperty(False)

    def on_values(self, obj, values):
        self.input_element.clear_elements()
        self.input_element.add_elements([
                Option(value=item, content=item) for item in values])


class Combobox(BaseCombobox):
    pass


class FloatingCombobox(BaseCombobox):
    pass

