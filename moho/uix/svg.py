"""
SVG
===

"""

from moho.core.properties import (
    NumericProperty, ListProperty,
    StringProperty, ReferenceListProperty)
from moho.uix.element import HtmlElement, Element
from moho.core.attributes import (
    ElementNumericAttribute, EventAttribute,
    StyleAttribute, StyleNumericAttribute,
    StyleColorAttribute, ElementAttribute)
from moho.core.event import EventDispatcher


class SvgElement(Element):
    """
    SvgElement class, represents html <svg> element
    """

    tabindex = ElementNumericAttribute(None, units="")

    alignment_baseline = StyleAttribute(
        None, options=[
            "auto",
            "baseline",
            "before-edge",
            "text-before-edge",
            "middle",
            "central",
            "after-edge",
            "text-after-edge",
            "ideographic",
            "alphabetic",
            "hanging",
            "mathematical",
            "inherit"
        ])

    baseline_shift = StyleNumericAttribute(
        None, options=["auto", "baseline", "super", "inherit"])

    color = StyleColorAttribute(None, options=["inherit"])

    cursor = StyleAttribute(
        None, options=["auto", "crosshair", "default",
                       "pointer", "move", "e-resize",
                       "nw-resize", "n-resize", "se-resize",
                       "sw-resize", "w-resize", "text",
                       "wait", "help", "inherit"], remote=True)

    fill = StyleAttribute(None, remote=True)

    fill_opacity = StyleNumericAttribute(
        None, options=["inherit"], remote=True)

    opacity = StyleNumericAttribute(
        None, options=["inherit"], remote=True)

    display = StyleAttribute(
        None, options=["inline", "block", "list-item",
                       "run-in", "compact", "marker",
                       "table", "inline-table", "table-row-group",
                       "table-header-group", "table-footer-group",
                       "table-row",
                       "table-column-group", "table-column",
                       "table-cell",
                       "table-caption", "none", "inherit"])

    stroke = StyleAttribute(None, remote=True)

    stroke_linecap = StyleAttribute(
        None, options=[
            "butt", "round", "square", "inherit"
        ], remote=True)

    stroke_linejoin = StyleAttribute(
        None, options=[
            "miter", "round", "bevel", "inherit"
        ], remote=True)

    stroke_opacity = StyleNumericAttribute(
        None, options=["inherit"], remote=True)

    stroke_width = StyleNumericAttribute(
        None, options=["inherit"], remote=True)

    visibility = StyleAttribute(
        None, options=["visible", "hidden", "collapse", "inherit"], remote=True)

    onactivate = EventAttribute(False)

    onclick = EventAttribute(False)

    onfocusin = EventAttribute(False)

    onfocusout = EventAttribute(False)

    onload = EventAttribute(False)

    onmousedown = EventAttribute(False)

    onmousemove = EventAttribute(False)

    onmouseout = EventAttribute(False)

    onmouseover = EventAttribute(False)

    onmouseup = EventAttribute(False)

    def on_remote_activate(self, **kwargs):
        pass

    def on_remote_click(self, **kwargs):
        pass

    def on_remote_focusin(self, **kwargs):
        pass

    def on_remote_focusout(self, **kwargs):
        pass

    def on_remote_load(self, **kwargs):
        pass

    def on_remote_mousedown(self, **kwargs):
        pass

    def on_remote_mousemove(self, **kwargs):
        pass

    def on_remote_mouseout(self, **kwargs):
        pass

    def on_remote_mouseover(self, **kwargs):
        pass

    def on_remote_mouseup(self, **kwargs):
        pass


class SvgTransformBehavior(object):

    transform_instructions = ListProperty([])

    transform_attr = ElementAttribute(None, attrname="transform")

    def add_transform(self, instr):
        instr.bind(command=self.update_transform)
        self.transform_instructions.append(instr)
        self.update_transform()

    def remove_transform(self, instr):
        instr.unbind(command=self.update_transform)
        self.transform_instructions.remove(instr)
        self.update_transform()

    def clear_transform(self):
        for tr in self.transform_instructions:
            tr.unbind(command=self.update_transform)
        self.transform_instructions.clear()
        self.update_transform()

    def update_transform(self, *args):
        self.transform_attr = " ".join(
            [item.command for item in self.transform_instructions])


class SvgTransformInstruction(EventDispatcher):

    command = StringProperty('')

    def __init__(self, **kwargs):
        super(SvgTransformInstruction, self).__init__(**kwargs)
        self.update_command()

    def update_command(self, *args):
        pass


class SvgMatrixTransform(SvgTransformInstruction):

    a = NumericProperty(0)

    b = NumericProperty(0)

    c = NumericProperty(0)

    d = NumericProperty(0)

    e = NumericProperty(0)

    f = NumericProperty(0)

    matrix = ReferenceListProperty(a, b, c, d, e, f)

    def update_command(self, *args):
        self.command = "matrix({}, {}, {}, {}, {}, {})".format(*self.matrix)

    def on_matrix(self, *args):
        self.update_command(*args)


class SvgTranslateTransform(SvgTransformInstruction):

    x = NumericProperty(0)

    y = NumericProperty(0)

    pos = ReferenceListProperty(x, y)

    def update_command(self, *args):
        self.command = "translate({}, {})".format(*self.pos)

    def on_pos(self, *args):
        self.update_command(*args)


class SvgScaleTransform(SvgTransformInstruction):

    x = NumericProperty(0)

    y = NumericProperty(None, allownone=True)

    scale = ReferenceListProperty(x, y)

    def update_command(self, *args):
        y = self.x if self.y is None else self.y
        self.command = "scale({}, {})".format(self.x, y)

    def on_scale(self, *args):
        self.update_command(*args)


class SvgRotateTransform(SvgTransformInstruction):

    angle = NumericProperty(0)

    x = NumericProperty(None, allownone=True)

    y = NumericProperty(None, allownone=True)

    pos = ReferenceListProperty(x, y)

    def update_command(self, *args):
        if self.x is None and self.y is None:
            self.command = "rotate({})".format(self.angle)
        elif self.x is not None and self.y is not None:
            self.command = "rotate({} {} {})".format(self.angle, *self.pos)
        elif self.y is None and self.x is not None:
            self.command = "rotate({} {})".format(self.angle, self.x, 0.0)
        else:
            raise ValueError('Wrong position parameter,'
            ' expected (None, None) or (float, float)')

    def on_angle(self, *args):
        self.update_command(*args)

    def on_pos(self, *args):
        self.update_command(*args)


class SvgSkewXTransform(SvgTransformInstruction):

    angle = NumericProperty(0)

    def update_command(self, *args):
            self.command = "skewX({})".format(self.angle)

    def on_angle(self, *args):
        self.update_command(*args)


class SvgSkewYTransform(SvgTransformInstruction):

    angle = NumericProperty(0)

    def update_command(self, *args):
            self.command = "skewY({})".format(self.angle)

    def on_angle(self, *args):
        self.update_command(*args)


class SvgRectangle(SvgElement, SvgTransformBehavior):

    tagname = "rect"

    x = ElementNumericAttribute(0, remote=True)

    y = ElementNumericAttribute(0, remote=True)

    width = ElementNumericAttribute(100, remote=True)

    height = ElementNumericAttribute(100, remote=True)

    rx = ElementNumericAttribute(0, remote=True)

    ry = ElementNumericAttribute(0, remote=True)


class SvgCircle(SvgElement, SvgTransformBehavior):

    tagname = "circle"

    cx = ElementNumericAttribute(0, remote=True)

    cy = ElementNumericAttribute(0, remote=True)

    r = ElementNumericAttribute(100, remote=True)


class SvgLine(SvgElement, SvgTransformBehavior):

    tagname = "line"

    x1 = ElementNumericAttribute(0, remote=True)

    x2 = ElementNumericAttribute(0, remote=True)

    y1 = ElementNumericAttribute(100, remote=True)

    y2 = ElementNumericAttribute(100, remote=True)


class SvgDefs(SvgElement, SvgTransformBehavior):

    tagname = "defs"


class SvgUse(SvgElement, SvgTransformBehavior):

    tagname = "use"

    x = ElementNumericAttribute(0, remote=True)

    y = ElementNumericAttribute(0, remote=True)

    width = ElementNumericAttribute(100, remote=True)

    height = ElementNumericAttribute(100, remote=True)


class SvgGroup(SvgElement, SvgTransformBehavior):

    tagname = "g"


class SvgHyperLink(SvgElement):

    tagname = "a"

    href = ElementAttribute('#', remote=True)

    target = ElementAttribute(
        None,
        options=["blank", "parent", "self", "top"],
        values=["_blank", "_parent", "_self", "_top"]
    )


class SvgImage(SvgElement, SvgTransformBehavior):

    tagname = "image"

    x = ElementNumericAttribute(0, remote=True)

    y = ElementNumericAttribute(0, remote=True)

    width = ElementNumericAttribute(100, remote=True)

    height = ElementNumericAttribute(100, remote=True)

    href = ElementAttribute('', remote=True)


class SvgText(SvgElement, SvgTransformBehavior):

    tagname = "text"

    x = ElementNumericAttribute(0, remote=True)

    y = ElementNumericAttribute(0, remote=True)

    dx = ElementNumericAttribute(0, remote=True)

    dy = ElementNumericAttribute(0, remote=True)

    text_anchor = ElementAttribute(
        None, options=["start", "middle", "end", "inherit"], remote=True)

    text_length = ElementNumericAttribute(
        None, attrname="textLength", remote=True)


class SvgView(SvgElement):

    tagname = "view"

    viewbox = ElementAttribute(None, attrname="viewBox", remote=True)


class Svg(HtmlElement):

    tagname = "svg"

    viewbox = ElementAttribute(None, attrname="viewBox", remote=True)

    __accepted_children_types__ = [SvgElement]

