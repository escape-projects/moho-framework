"""
Body
====

The `Body` class represents HTML body element and together with the `Application` class forms the base of any Moho application. 
In other words the body element is is the root element for any application and should be returned by an overriden `build` method.

Here is an example of a minimum application, which shows blank page::

    class MainBody(Body):
        pass

    class ShowApp(BootstrapApp):
    
        def build(self):
            return MainBody()
    
    
    if __name__ == "__main__":
    
        Logger.setLevel('DEBUG')
    
        ShowApp(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        ).run()

"""

from moho.uix.element import HtmlElement
from moho.core.properties import StringProperty
from moho.core.attributes import EventAttribute


class Body(HtmlElement):
    """
    Body class, represents root HTML body element.
    """

    tagname = "body"
    
    onafterprint = EventAttribute(False)
    '''
    Remote event which is triggered when a page has started printing.
    '''
    
    onbeforeprint = EventAttribute(False)
    '''
    Remote event which is triggered when a page is about to be printed.
    '''

    onbeforeunload = EventAttribute(False)
    '''
    Remote event which is triggered when a page is about to be unloaded.
    '''

    onerror = EventAttribute(False)
    """
    Remote event which is triggered when an error occurs.
    """

    onhashchange = EventAttribute(False)
    """
    Remote event which is triggered when there has been changes to the anchor part of the a URL.
    """

    onpageshow = EventAttribute(False)
    """
    Remote event which is triggered when a user navigates to a page.
    """

    onpagehide = EventAttribute(False)
    """
    Remote event which is triggered when a user navigates away from a page.
    """

    onload = EventAttribute(False)
    """
    Remote event which is triggered after the page is finished loading.
    """
    
    onunload = EventAttribute(False)
    """
    Remote event which is triggered once a page has unloaded (or the browser window has been closed).
    """

    onoffline = EventAttribute(False)
    """
    Remote event which is triggered when the browser starts to work offline.
    """

    ononline = EventAttribute(False)
    """
    Remote event which is triggered when the browser starts to work online.
    """

    onresize = EventAttribute(
        False,
        eventargs={"clientWidth": "window.ClientWidth",
                   "clientHeight": "window.ClientHeight",
                   "innerWidth": "window.innerWidth",
                   "innerHeight": "window.innerHeight",
                   "outerWidth": "window.outerWidth",
                   "outerHeight": "window.outerHeight",
                   "offsetWidth": "window.offsetWidth",
                   "offsetHeight": "window.offsetHeight"}
    )
    """
    Remote event which is triggered when the browser window is resized.
    """

    def on_remote_afterprint(self, **kwargs):
        pass

    def on_remote_beforeprint(self, **kwargs):
        pass

    def on_remote_beforeunload(self, **kwargs):
        pass

    def on_remote_error(self, **kwargs):
        pass

    def on_remote_hashchange(self, **kwargs):
        pass

    def on_remote_load(self, **kwargs):
        pass

    def on_remote_offline(self, **kwargs):
        pass

    def on_remote_online(self, **kwargs):
        pass

    def on_remote_resize(self, **kwargs):
        pass

    def on_remote_unload(self, **kwargs):
        pass

    def on_remote_pageshow(self, **kwargs):
        pass

    def on_remote_pagehide(self, **kwargs):
        pass

    html_tpl = StringProperty("""
   <{{obj.tagname}} id="{{obj.tagid}}"
        {% if obj.cls %} class="{{obj.cls}}" {% end %}
        {% if obj.style %} style="{{obj.style}}" {% end %}
        {% raw obj.attributes %}>
        {% for url in obj.application.scripts %}
            <script src="{{url}}"></script>
        {%end%}

        {%raw obj.content%}

        <script >

            $(document).ready(
                function(){
                moho.main_socket.connect();
            });

        </script>

    </{{obj.tagname}}>
    """)
