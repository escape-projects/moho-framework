"""
Moho UIX classes
================

Most of the user interface classes in moho are based on `HtmlElement` class, which represents usually an atomic
html5 element or
can consist of several elements defining more complex components.
The `moho.uix` package contains modules with classes for creating and managing such components.
Please refer to the :doc:`api-moho.uix.element` documentation for further
information.
Moho UIX elements are assempbled into several packages as follows:

- **HTML5 UX elements**: Classical HTML5 elements, ready to be assembled into
  more complex widgets. This set of elements is raw and doesn't contain any layout class.
  Development of applications with HTML5 package only is possible but will require 
  a lot of styling work.
    
- **Bootstrap UX elements**: Components and elements created using 
  `Bootstrap library <https://getbootstrap.com/>`_. This set of elements is more advanced and has 
  components like layouts, progressbars, alerts, accordion etc.
    
----
"""
