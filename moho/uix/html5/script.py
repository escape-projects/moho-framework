"""
Script
======


"""

from moho.core.properties import (
    StringProperty, AliasProperty)
from moho.uix.element import HtmlElement
from moho.core.attributes import ElementAttribute
from tornado.template import Loader, Template


class Script(HtmlElement):
    """
    Script class, represents html <script> element
    """

    tagname = "script"

    async_attr = ElementAttribute(None, options=["async"], attrname="async")

    charset = ElementAttribute(None)

    defer = ElementAttribute(None, options=["defer"])

    template = StringProperty(None)

    namespace = StringProperty(None)

    src = ElementAttribute(None)

    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" '
        '{% raw obj.attributes %} >'
        '{%raw obj.content%}</{{obj.tagname}}>'
    )

    def get_content(self):
        app = self.application
        tpl = self.template
        if not self.src and tpl:
            if tpl.endswith(".js"):
                loader = Loader(app.template_path)
                return loader.load(tpl).generate(app=app, element=self)
            else:
                return Template(tpl).generate(app=app, element=self)
        return ''

    content = AliasProperty(get_content, None)
