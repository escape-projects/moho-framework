"""
Image
=====


"""


from moho.uix.element import NoContentHtmlElement
from moho.core.attributes import (
    ElementAttribute, ElementNumericAttribute, EventAttribute)


class Image(NoContentHtmlElement):
    """
    Image class, represents html <img> element
    """

    tagname = "img"

    src = ElementAttribute('', remote=True)
    """
    Specifies the path to the image
    """

    alt = ElementAttribute(None)
    """
    Specifies an alternate text for an image
    """

    height_attr = ElementNumericAttribute(
        None, attrname="height", remote=True)
    """Specifies the height of an image"""
    

    width_attr = ElementNumericAttribute(
        None, attrname="width", remote=True)
    """Specifies the width of an image"""

    onerror = EventAttribute(False)
    """Fires when error occurs"""
    

    onload = EventAttribute(False)
    """Fires after the image is finished loading"""
    
    def on_remote_error(self, **kwargs):
        pass

    def on_remote_load(self, **kwargs):
        pass