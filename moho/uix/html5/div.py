"""
Div
===

"""

from moho.uix.element import HtmlElement


class Div(HtmlElement):
    """
    Div class, represents basic div element
    """
    
    tagname="div"
