"""
Input
=====

Set of classes representing various types of html inputs

"""

from moho.uix.element import HtmlElement, NoContentHtmlElement
from moho.core.properties import StringProperty
from moho.core.attributes import (
    ElementAttribute, ElementNumericAttribute,
    EventAttribute, ElementBooleanAttribute)


class InputLabel(HtmlElement):
    """
    Label class, represents html label element
    """
    
    text = StringProperty('')
    
    tagname = "label"

    forattr = ElementAttribute(None)



class InputBase:
    
    type = ElementAttribute("text", 
                            options=[
                                "text",
                                "button",
                                "checkbox",
                                "color",
                                "date",
                                "datetime-local",
                                "email",
                                "url",
                                "file", 
                                "month",
                                "number",
                                "password",
                                "radio",
                                "range",
                                "reset",
                                "tel",
                                "time"
                            ])
    
    autocomplete = ElementAttribute(None, options=['on', 'off'])

    autofocus = ElementBooleanAttribute(False)

    disabled = ElementBooleanAttribute(False, remote=True)

    form = ElementAttribute(None)

    formonvalidate = ElementBooleanAttribute(False)

    name = ElementAttribute(None)

    pattern = ElementAttribute(None)

    placeholder = ElementAttribute(None)

    readonly = ElementBooleanAttribute(False, remote=True)

    required = ElementBooleanAttribute(False, remote=True)
    
    checked = ElementBooleanAttribute(False, remote=True)
    
    value = ElementAttribute(None, remote=True)
    
    maxlength = ElementNumericAttribute(None, remote=True)

    size = ElementNumericAttribute(None, remote=True)
    
    max = ElementNumericAttribute(None, remote=True, units="")

    min = ElementNumericAttribute(None, remote=True, units="")

    step = ElementNumericAttribute(None, remote=True, units="")


    multiple = ElementBooleanAttribute(False, remote=True)

    oninvalid = EventAttribute(
        False, eventargs={"value": "this.value"})
    
    onchange = EventAttribute(
        False, eventargs={"checked": "this.checked",
                          "value": "this.value"
    })
    
    oninput = EventAttribute(False, eventargs={"value": "this.value"})
    
    def on_remote_change(self, **kwargs):
        if self.type in ["checkbox", "radio"]:
            self.checked = kwargs.get("checked", False)
        else:
            self.value = kwargs.get("value", False)

    def on_remote_input(self, **kwargs):
        self.value = kwargs.get("value", False)

    
    def on_remote_invalid(self, **kwargs):
        pass


class Input(InputBase, NoContentHtmlElement):
    """
    Input class, base input class for all other input variants
    """

    tagname = "input"
    

