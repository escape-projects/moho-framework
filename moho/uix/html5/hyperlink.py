"""
Hyperlink
=========


"""

from moho.uix.element import HtmlElement
from moho.core.attributes import ElementAttribute


class HyperLinkBase(object):
    
    href = ElementAttribute('#', remote=True)
    """
    Specifies the URL of the page the link goes to
    """

    download = ElementAttribute(None)
    """
    Specifies that the target will be downloaded when a user clicks on the hyperlink
    """

    target = ElementAttribute(
        None,
        options=["blank", "parent", "self", "top"],
        values=["_blank", "_parent", "_self", "_top"]
    )
    """
    Specifies where to open the linked document
    """



class HyperLink(HyperLinkBase, HtmlElement):
    """
    HyperLink class, represents html anchor element
    """

    tagname = "a"
