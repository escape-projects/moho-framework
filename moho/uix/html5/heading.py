"""
Heading
=======

Set of H1 to H6 classes representing html header elements of corresponding level.


"""

__all__ = ("H1", "H2", "H3", "H4", "H5", "H6")

from moho.uix.element import HtmlElement


class Heading(HtmlElement):
    pass


class H1(Heading):
    
    tagname = "h1"


class H2(Heading):
    
    tagname = "h2"


class H3(Heading):
    
    tagname = "h3"


class H4(Heading):
    
    tagname = "h4"


class H5(Heading):
    
    tagname = "h5"


class H6(Heading):
    
    tagname = "h6"
