"""
Textarea
========

"""
from moho.core.properties import StringProperty
from moho.core.attributes import ElementNumericAttribute,\
    ElementAttribute, ElementBooleanAttribute, EventAttribute
from moho.uix.element import HtmlElement


class TextAreaBase(object):
    
    autofocus = ElementBooleanAttribute(False)

    cols = ElementNumericAttribute(None, remote=True)

    disabled = ElementBooleanAttribute(False, remote=True)

    form = ElementAttribute(None)

    maxlength = ElementNumericAttribute(None, remote=True)

    name = ElementAttribute(None)

    placeholder = ElementAttribute(None)

    readonly = ElementBooleanAttribute(False, remote=True)

    rows = ElementNumericAttribute(None, remote=True)

    wrap = ElementAttribute(None, options=["hard", "soft"], remote=True)
    
    value = StringProperty('')
    
    onchange = EventAttribute(
        False, eventargs={"value": "this.value"})
    
    oninput = EventAttribute(
        False, eventargs={"value": "this.value"})
    
    js_tpl = StringProperty(
        "$('#{{obj.tagid}}').on('on_select', null, function(e, args)"
        "{$('#{{obj.tagid}}').val(args.value);});"
    )
    
    
    def on_remote_change(self, value, **kwargs):
        self.value = value

    def on_remote_input(self, value, **kwargs):
        self.value = value
        
        
class TextArea(TextAreaBase, HtmlElement):
    """
    TextArea class, represents html <textarea> element
    """

    tagname = "textarea"
