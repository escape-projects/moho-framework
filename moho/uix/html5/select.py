"""
Select
======

Contains html select and related option and optgroup classes


"""

from moho.uix.element import HtmlElement
from moho.core.attributes import ElementAttribute,\
    ElementNumericAttribute, ElementBooleanAttribute, EventAttribute
from moho.core.properties import StringProperty


class Option(HtmlElement):
    """
    Option class, represents html <option> element
    """

    tagname = "option"

    disabled = ElementBooleanAttribute(False, remote=True)

    label = ElementAttribute(None, remote=True)

    selected = ElementBooleanAttribute(False, remote=True)

    value = ElementAttribute(None, remote=True)


class OptionGroup(HtmlElement):
    """
    OptionGroup class, represents html <optgroup> element
    """

    tagname = "optgroup"

    disabled = ElementBooleanAttribute(False, remote=True)

    label = ElementAttribute(None, remote=True)


class SelectBase:
    
    js_tpl = StringProperty("""

    $('#{{obj.tagid}}').on("on_select", null, function(e, args){
        $('#{{obj.tagid}}').val(args.value);
    });
    
    """)
    
    
    autofocus = ElementBooleanAttribute(False)

    disabled = ElementBooleanAttribute(False, remote=True)

    form = ElementAttribute(None)

    multiple = ElementBooleanAttribute(False, remote=True)

    name = ElementAttribute(None)

    required = ElementBooleanAttribute(False)

    size = ElementNumericAttribute(None, remote=True)

    value = StringProperty('')

    onchange = EventAttribute(
        False, eventargs={"value": "this.value"})

    def on_remote_change(self, value, **kwargs):
        self.value = value
        for el in self.children:
            if isinstance(el, Option):
                el.selected = (el.value==value)

    def on_value(self, instance, val):
        self.remote_dispatch("on_select", value=val)
        


class Select(SelectBase, HtmlElement):
    """
    Select class, represents html <select> element
    """
    
    tagname = "select"

