
from moho.uix.x3d.element import X3dElement
from moho.core.attributes import (
    ElementLogicalAttribute, ElementListAttribute)


class X3dScene(X3dElement):
    tagname = "Scene"


class X3dTransform(X3dElement):

    tagname = "Transform"

    bbox_center = ElementListAttribute(
        None, length=3, attrname="bboxCenter",
        remote=True
    )

    bbox_size = ElementListAttribute(
        None, length=3, attrname="bboxsize", remote=True
    )

    center = ElementListAttribute(None, length=3, remote=True)

    render = ElementLogicalAttribute(None, remote=True)

    rotation = ElementListAttribute(None, length=4, remote=True)

    scale = ElementListAttribute(None, length=3, remote=True)

    scale_orientation = ElementListAttribute(
        None, length=4, attrname="scaleOrientation", remote=True)

    translation = ElementListAttribute(None, length=3, remote=True)
