"""
X3D UIX classes
================
"""

from moho.core.properties import StringProperty
from moho.uix.element import HtmlElement
from moho.core.attributes import ElementNumericAttribute


class X3d(HtmlElement):

    tagname = "x3d"

    script = StringProperty("http://www.x3dom.org/release/x3dom-full.js")

    stylesheet = StringProperty("http://www.x3dom.org/release/x3dom.css")
